package applepiedev.com.padelstats.lib.eventbus;

/**
 * Created by Daniel on 17/11/2016.
 */

public interface Eventbus {
    void register(Object subscriber);
    void unregister(Object subscriber);
    void post(Object event);
}
