package applepiedev.com.padelstats.lib.eventbus;


import org.greenrobot.eventbus.EventBus;

/**
 * Created by Daniel on 17/11/2016.
 */

public class GreenRobotEventbus implements Eventbus {

    EventBus eventbus;
    private static class SingletonHolder{
        private static final GreenRobotEventbus INSTANCE = new GreenRobotEventbus();
    }

    public static GreenRobotEventbus getInstance(){
        return SingletonHolder.INSTANCE;
    }

    public GreenRobotEventbus() {
        this.eventbus = EventBus.getDefault();
    }

    @Override
    public void register(Object subscriber) {
        eventbus.register(subscriber);
    }

    @Override
    public void unregister(Object subscriber) {
        eventbus.unregister(subscriber);
    }

    @Override
    public void post(Object event) {
        eventbus.post(event);
    }
}
