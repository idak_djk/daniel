package applepiedev.com.padelstats.lib.retrofit;


import rx.Subscriber;

public abstract class ApiCallback<M> extends Subscriber<M> {

    public abstract void onSuccess(M model);

    public abstract void onFailure(String error);

    public abstract void onFinish();


    @Override
    public void onError(Throwable e) {
        String aux = "";
        onFailure(e.getMessage());
        //onFinish();
    }

    @Override
    public void onNext(M model) {
        onSuccess(model);
    }

    @Override
    public void onCompleted() {
        onFinish();
    }


}
