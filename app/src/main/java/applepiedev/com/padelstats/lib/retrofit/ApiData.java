package applepiedev.com.padelstats.lib.retrofit;

import java.util.List;

import applepiedev.com.padelstats.Models.GlobalsModel;
import applepiedev.com.padelstats.Models.LoginModel;
import applepiedev.com.padelstats.Models.RegisterModel;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Daniel on 24/02/2017.
 */

public interface ApiData {
    @FormUrlEncoded
    @POST("querys.php")
    Observable<List<GlobalsModel>> getData(@Field("function") String function,
                                           @Field("userId") String userId);


    @FormUrlEncoded
    @POST("login.php")
    Observable<List<LoginModel>> doLogin(@Field("email") String email,
                                         @Field("pass") String pass);

    @FormUrlEncoded
    @POST("signin.php")
    Observable<String> doRegister(@Field("name") String name,
                                               @Field("email") String email,
                                               @Field("pass") String pass);

    @FormUrlEncoded
    @POST("querys.php")
    Observable<List<String>> addData(@Field("function") String function,
                                     @Field("userId") String userId,
                                     @Field("ataque") Float ataque,
                                     @Field("defensa") Float defensa,
                                     @Field("transicion") Float transicion,
                                     @Field("saque") Float saque,
                                     @Field("posicion") Float posicion,
                                     @Field("resultado") int resultado,
                                     @Field("day") int day,
                                     @Field("month") int month,
                                     @Field("year") int year);
}
