package applepiedev.com.padelstats.Register.implementations;

import applepiedev.com.padelstats.Models.RegisterModel;
import applepiedev.com.padelstats.Register.events.RegisterEvents;
import applepiedev.com.padelstats.Register.interfaces.RegisterPresenter;
import applepiedev.com.padelstats.Register.interfaces.RegisterRepository;
import applepiedev.com.padelstats.Register.interfaces.RegisterView;
import applepiedev.com.padelstats.lib.eventbus.Eventbus;
import applepiedev.com.padelstats.lib.eventbus.GreenRobotEventbus;

/**
 * Created by idak_ on 07/05/2017.
 */

public class RegisterPresenterImpl implements RegisterPresenter{

    private Eventbus eventbus;
    private RegisterView registerView;
    private RegisterRepository registerRepository;

    public RegisterPresenterImpl(RegisterView registerView) {
        this.registerView = registerView;
        eventbus = GreenRobotEventbus.getInstance();
        registerRepository = new RegisterRepositoryImpl();
    }

    @Override
    public void onCreate() {
        eventbus.register(this);
    }

    @Override
    public void onDestroy() {
        eventbus.unregister(this);
    }

    @Override
    public void doRegister(RegisterModel registerModel) {
        registerRepository.doRegister(registerModel);
    }

    @Override
    public void doRegisterSuccess() {
        registerView.doRegisterSuccess();
    }

    @Override
    public void doRegisterError() {
        registerView.doRegisterError();
    }

    @Override
    public void onEventMainThread(RegisterEvents event) {
        switch (event.getEventType()) {
            case RegisterEvents.ondoRegisterSuccess:
                doRegisterSuccess();
                break;
            case RegisterEvents.ondoRegisterError:
                doRegisterError();
                break;
        }
    }
}
