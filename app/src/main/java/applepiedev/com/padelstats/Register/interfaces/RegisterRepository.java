package applepiedev.com.padelstats.Register.interfaces;

import applepiedev.com.padelstats.Models.RegisterModel;

/**
 * Created by idak_ on 07/05/2017.
 */

public interface RegisterRepository {
    void doRegister(RegisterModel registerModel);
}
