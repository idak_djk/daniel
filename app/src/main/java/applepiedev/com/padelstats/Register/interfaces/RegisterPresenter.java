package applepiedev.com.padelstats.Register.interfaces;

import applepiedev.com.padelstats.Models.RegisterModel;
import applepiedev.com.padelstats.Register.events.RegisterEvents;

/**
 * Created by idak_ on 07/05/2017.
 */

public interface RegisterPresenter {
    void onCreate();
    void onDestroy();

    void doRegister(RegisterModel registerModel);
    void doRegisterSuccess();
    void doRegisterError();

    // =========== Events =============
    void onEventMainThread(RegisterEvents event);
}
