package applepiedev.com.padelstats.Register.implementations;

import android.util.Log;

import java.util.List;

import applepiedev.com.padelstats.Models.RegisterModel;
import applepiedev.com.padelstats.Register.events.RegisterEvents;
import applepiedev.com.padelstats.Register.interfaces.RegisterRepository;
import applepiedev.com.padelstats.lib.eventbus.Eventbus;
import applepiedev.com.padelstats.lib.eventbus.GreenRobotEventbus;
import applepiedev.com.padelstats.lib.retrofit.ApiCallback;
import applepiedev.com.padelstats.lib.retrofit.ApiData;
import applepiedev.com.padelstats.lib.retrofit.AppClient;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by idak_ on 07/05/2017.
 */

public class RegisterRepositoryImpl implements RegisterRepository{
    public ApiData apiData = AppClient.retrofit().create(ApiData.class);
    private CompositeSubscription mCompositeSubscription;
    @Override
    public void doRegister(RegisterModel registerModel) {
        addSubscription(apiData.doRegister(registerModel.getFullname(),registerModel.getEmail(),registerModel.getPass()),
                new ApiCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        postEvent(RegisterEvents.ondoRegisterSuccess);
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e("ERROR", error);
                        postEvent(RegisterEvents.ondoRegisterError);
                    }
                    @Override
                    public void onFinish() {
                        // mvpView.hideLoading();
                    }
                });
    }

    public void addSubscription(Observable observable, Subscriber subscriber) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        mCompositeSubscription.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber));
    }

    private void postEvent(List<RegisterModel> result) {
        RegisterEvents registerEvents = new RegisterEvents();
        registerEvents.setEventType(RegisterEvents.ondoRegisterSuccess);
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(registerEvents);
    }

    private void postEvent(int eventType) {
        RegisterEvents registerEvents = new RegisterEvents();
        registerEvents.setEventType(eventType);
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(registerEvents);
    }

}
