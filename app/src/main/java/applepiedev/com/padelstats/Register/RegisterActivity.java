package applepiedev.com.padelstats.Register;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import applepiedev.com.padelstats.Models.RegisterModel;
import applepiedev.com.padelstats.R;
import applepiedev.com.padelstats.Register.implementations.RegisterPresenterImpl;
import applepiedev.com.padelstats.Register.interfaces.RegisterPresenter;
import applepiedev.com.padelstats.Register.interfaces.RegisterView;

public class RegisterActivity extends AppCompatActivity implements RegisterView{

    private RegisterPresenter registerPresenter;
    private RegisterModel registerModel;
    private EditText reg_fullname;
    private EditText reg_email;
    private EditText reg_password;
    private Button btn_reg_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        reg_fullname = (EditText) findViewById(R.id.reg_fullname);
        reg_email = (EditText) findViewById(R.id.reg_email);
        reg_password = (EditText) findViewById(R.id.reg_password);
        btn_reg_email = (Button) findViewById(R.id.btn_reg_email);
        btn_reg_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickRegister();
            }
        });

        // New register model to complete
        registerModel = new RegisterModel();
        // New presenter to send data
        registerPresenter = new RegisterPresenterImpl(this);
    }

    @Override
    public void clickRegister() {
        //TODO Complete model, do validations and then doRegister
        String fullname = reg_fullname.getText().toString();
        String email = reg_email.getText().toString();
        String pass = reg_password.getText().toString();
        registerModel.setFullname(fullname);
        registerModel.setEmail(email);
        registerModel.setPass(pass);
        doRegister(registerModel);
    }

    @Override
    public void doRegister(RegisterModel registerModel) {
        registerPresenter.doRegister(registerModel);
    }

    @Override
    public void doRegisterSuccess() {
        // TODO RegisterSuccess
        Log.e("REGISTER","SUCCESS");
    }

    @Override
    public void doRegisterError() {
        // TODO Register Error
        Log.e("REGISTER","ERROR");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        registerPresenter.onDestroy();
    }
}
