package applepiedev.com.padelstats.Register.events;

/**
 * Created by idak_ on 07/05/2017.
 */

public class RegisterEvents {
    public static final int ondoRegisterSuccess = 1;
    public static final int ondoRegisterError = 2;

    private int eventType;
    private String errorMessage;

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
