package applepiedev.com.padelstats.Drawer;

/**
 * Created by RSA on 23/11/2016.
 */

public class Partidos {
    private int id;
    private int resultado;
    private int day;
    private int month;
    private int year;
    private Float ataque, defensa, transicion, saque, posicion;
    private String hora;


    //Definimos los getters y setters para acceder a la info del objeto partido

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Float getAtaque() {
        return ataque;
    }

    public void setAtaque(Float ataque) {
        this.ataque = ataque;
    }

    public Float getDefensa() {
        return defensa;
    }

    public void setDefensa(Float defensa) {
        this.defensa = defensa;
    }

    public Float getTransicion() {
        return transicion;
    }

    public void setTransicion(Float transicion) {
        this.transicion = transicion;
    }

    public Float getSaque() {
        return saque;
    }

    public void setSaque(Float saque) {
        this.saque = saque;
    }

    public Float getPosicion() {
        return posicion;
    }

    public void setPosicion(Float posicion) {
        this.posicion = posicion;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}
