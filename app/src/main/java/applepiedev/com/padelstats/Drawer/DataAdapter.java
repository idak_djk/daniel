package applepiedev.com.padelstats.Drawer;

/**
 * Created by RSA on 12/09/2016.
 */

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import applepiedev.com.padelstats.Circulo.CircleProgressBar;
import applepiedev.com.padelstats.R;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<Partidos> partidos_array;
    private View.OnClickListener listener;
    private Typeface tf;

    public DataAdapter(ArrayList<Partidos> partidos_array) {
        this.partidos_array = partidos_array;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        //inflamos con el xml itemlista
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.historial_item, viewGroup, false);
        tf= Typeface.createFromAsset(view.getContext().getAssets(), "arista.ttf");
        //Incluimos opción del listener
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int i) {
        //Capturamos cada una de las propiedades de los objetos
        int res = partidos_array.get(i).getResultado();
        float ata = partidos_array.get(i).getAtaque();
        float def = partidos_array.get(i).getDefensa();
        float tra = partidos_array.get(i).getTransicion();
        float saq = partidos_array.get(i).getSaque();
        float pos = partidos_array.get(i).getPosicion();
        float global = (ata + def + tra + saq + pos)/5;
        viewHolder.txtDay.setText(Integer.toString(partidos_array.get(i).getDay()) + "/");
        viewHolder.txtMonth.setText(Integer.toString(partidos_array.get(i).getMonth()) + "/");
        viewHolder.txtYear.setText(Integer.toString(partidos_array.get(i).getYear()));
        viewHolder.txtTime.setText(partidos_array.get(i).getHora() + "h");
        viewHolder.historial_card_ataque.setText(String.format("%.1f", ata)+"%");
        viewHolder.historial_card_defensa.setText(String.format("%.1f", def)+"%");
        viewHolder.historial_card_transicion.setText(String.format("%.1f", tra)+"%");
        viewHolder.historial_card_saque.setText(String.format("%.1f", saq)+"%");
        viewHolder.historial_card_posicion.setText(String.format("%.1f", pos)+"%");
        viewHolder.txtGlobal.setText(String.format("%.1f", global)+"%");

        viewHolder.txtDay.setTypeface(tf);
        viewHolder.txtMonth.setTypeface(tf);
        viewHolder.txtYear.setTypeface(tf);
        viewHolder.txtTime.setTypeface(tf);
        viewHolder.historial_card_ataque.setTypeface(tf);
        viewHolder.historial_card_defensa.setTypeface(tf);
        viewHolder.historial_card_transicion.setTypeface(tf);
        viewHolder.historial_card_saque.setTypeface(tf);
        viewHolder.historial_card_posicion.setTypeface(tf);
        viewHolder.txtGlobal.setTypeface(tf);

        switch (res) {
            case 1:
                viewHolder.circleProgressBar.setColor(Color.rgb(0,176,240));
                viewHolder.circleProgressBar.setProgressWithAnimation(100);
                viewHolder.circleProgressBar.setStrokeWidth(5);
                break;
            case 2:
                viewHolder.circleProgressBar.setColor(Color.rgb(255,0,0));
                viewHolder.circleProgressBar.setProgressWithAnimation(100);
                viewHolder.circleProgressBar.setStrokeWidth(5);
                break;
            default:
                viewHolder.circleProgressBar.setColor(Color.rgb(0,176,240));
                viewHolder.circleProgressBar.setProgressWithAnimation(100);
                viewHolder.circleProgressBar.setStrokeWidth(5);
                break;
        }
    }

    //Tamaño del array
    @Override
    public int getItemCount() {
        return partidos_array.size();
    }

    public Partidos getItem(int i) {
        return partidos_array.get(i);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null)
            listener.onClick(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtDay, txtMonth, txtYear, txtTime, txtGlobal, historial_card_ataque,
                historial_card_defensa, historial_card_transicion, historial_card_saque, historial_card_posicion;
        private CircleProgressBar circleProgressBar;

        public ViewHolder(View view) {
            super(view);
            //Declaración de views del xml
            txtGlobal = (TextView) view.findViewById(R.id.txtGlobal);
            txtDay = (TextView) view.findViewById(R.id.txtDay);
            txtMonth = (TextView) view.findViewById(R.id.txtMonth);
            txtYear = (TextView) view.findViewById(R.id.txtYear);
            txtTime = (TextView) view.findViewById(R.id.txtTime);
            circleProgressBar = (CircleProgressBar) view.findViewById(R.id.custom_progressBar);
            historial_card_ataque = (TextView) view.findViewById(R.id.historial_card_ataque);
            historial_card_defensa = (TextView) view.findViewById(R.id.historial_card_defensa);
            historial_card_transicion = (TextView) view.findViewById(R.id.historial_card_transicion);
            historial_card_saque = (TextView) view.findViewById(R.id.historial_card_saque);
            historial_card_posicion = (TextView) view.findViewById(R.id.historial_card_posicion);


        }
    }

}