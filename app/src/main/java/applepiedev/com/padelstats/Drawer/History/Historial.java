package applepiedev.com.padelstats.Drawer.History;


import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.Collections;

import applepiedev.com.padelstats.BBDD.SQLDataBase;
import applepiedev.com.padelstats.Drawer.Partidos;
import applepiedev.com.padelstats.R;
import applepiedev.com.padelstats.RadarMarkerView;

public class Historial extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DataAdapter adaptador;
    private SQLiteDatabase db;
    private String count = "SELECT count(*) FROM Estadisticas";
    private ArrayList<Partidos> array_partidos = new ArrayList<Partidos>();
    private String ataque, defensa, transicion, saque, position;
    private ProgressBar dialog_progressBarAtaque, dialog_progressBarDefensa,
            dialog_progressBarTransicion, dialog_progressBarSaque, dialog_progressBarPosicion;


    private RadarChart grafico_dialogo;
    public Typeface tf;
    public TextView titulo_dialogo, dialog_ataque, dialog_defensa, dialog_transicion,
            dialog_saque, dialog_posicion;
    public String[] golpes_nombres;
    private Float ata, def, tra, pos, saq;
    private int res, day, month, year;
    private String hora;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //handle the home button onClick event here.
                onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.historial);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        tf = Typeface.createFromAsset(getAssets(), "arista.ttf");
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        golpes_nombres = new String[]{getString(R.string.ataque), getString(R.string.defensa),
                getString(R.string.saque), getString(R.string.posicion), getString(R.string.transicion)};

        //==========================================================================
        // Consultamos si existen registros en la tabla de la BBDD
        //==========================================================================
        //Abrimos la base de datos 'DBEstadisticas' en modo escritura
        SQLDataBase sqldb = new SQLDataBase(this, "DBEstadisticas", null, 1);
        db = sqldb.getWritableDatabase();

        //Comprobamos si existen registros dentro de la tabla
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        //Si la base de datos no tiene registros
        if (icount == 0) {
            //Cambiar visibilidades de las views
        }

        //Si la base de datos si tiene registros
        else {
            //Muestro y oculto views


            //método rawQuery()
            Cursor c = db.rawQuery("SELECT id, ataque, defensa, transicion, saque, posicion, resultado, day, month, year, time FROM Estadisticas", null);
            //Recorremos los resultados para mostrarlos en pantalla
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya más registros
                do {
                    //Cogemos los datos del registro y los guardamos en variables
                    int id = c.getInt(0);
                    float ata = c.getFloat(1);
                    float def = c.getFloat(2);
                    float tra = c.getFloat(3);
                    float saq = c.getFloat(4);
                    float pos = c.getFloat(5);
                    int res = c.getInt(6);
                    int day = c.getInt(7);
                    int month = c.getInt(8);
                    int year = c.getInt(9);
                    String time = c.getString(10);
                    //Creamos el objeto partido para setearle los datos
                    Partidos partido = new Partidos();
                    //Seteo de datos
                    partido.setId(id);
                    partido.setAtaque(ata);
                    partido.setDefensa(def);
                    partido.setTransicion(tra);
                    partido.setSaque(saq);
                    partido.setPosicion(pos);
                    partido.setResultado(res);
                    partido.setDay(day);
                    partido.setMonth(month);
                    partido.setYear(year);
                    partido.setHora(time);
                    //Añadimos el objeto partido con sus datos del registro al array
                    array_partidos.add(partido);

                } while (c.moveToNext());
            }

        }
        //Orden inverso
        Collections.reverse(array_partidos);
        //Creamos objeto adaptador para que cada objeto pelicula tenga formato de cardview
        adaptador = new DataAdapter(array_partidos);
        //Introducimos en el recyclerview cada objeto
        recyclerView.setAdapter(adaptador);

        //Listener de pulsado en cada cardview
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Obtenemos datos del objeto pulsado
                int id = (recyclerView.getChildPosition(v));
                String posicion = String.valueOf(adaptador.getItem(id).getId());
                ata = adaptador.getItem(id).getAtaque();
                def = adaptador.getItem(id).getDefensa();
                tra = adaptador.getItem(id).getTransicion();
                saq = adaptador.getItem(id).getSaque();
                pos = adaptador.getItem(id).getPosicion();
                res = adaptador.getItem(id).getResultado();
                day = adaptador.getItem(id).getDay();
                month = adaptador.getItem(id).getMonth();
                year = adaptador.getItem(id).getYear();
                hora = adaptador.getItem(id).getHora();

                //Muestro dialogo
                final Dialog dialogo_historial = new Dialog(Historial.this, R.style.DialogTheme);
                dialogo_historial.setContentView(R.layout.dialogo_historial);
                dialogo_historial.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
                grafico_dialogo = (RadarChart) dialogo_historial.findViewById(R.id.grafico_dialogo);
                titulo_dialogo = (TextView) dialogo_historial.findViewById(R.id.main_title);

                dialog_ataque = (TextView) dialogo_historial.findViewById(R.id.dialog_ataque);
                dialog_defensa = (TextView) dialogo_historial.findViewById(R.id.dialog_defensa);
                dialog_transicion = (TextView) dialogo_historial.findViewById(R.id.dialog_transicion);
                dialog_saque = (TextView) dialogo_historial.findViewById(R.id.dialog_saque);
                dialog_posicion = (TextView) dialogo_historial.findViewById(R.id.dialog_posicion);

                dialog_progressBarAtaque = (ProgressBar) dialogo_historial.findViewById(R.id.dialog_progressBarAtaque);
                dialog_progressBarDefensa = (ProgressBar) dialogo_historial.findViewById(R.id.dialog_progressBarDefensa);
                dialog_progressBarTransicion = (ProgressBar) dialogo_historial.findViewById(R.id.dialog_progressBarTransicion);
                dialog_progressBarSaque = (ProgressBar) dialogo_historial.findViewById(R.id.dialog_progressBarSaque);
                dialog_progressBarPosicion = (ProgressBar) dialogo_historial.findViewById(R.id.dialog_progressBarPosicion);

                ataque = getString(R.string.ataque);
                defensa = getString(R.string.defensa);
                transicion = getString(R.string.transicion);
                saque = getString(R.string.saque);
                position = getString(R.string.posicion);

                dialog_ataque.setText(ataque + ": " + String.format("%.1f", ata)+"%");
                dialog_defensa.setText(defensa + ": " + String.format("%.1f", def)+"%");
                dialog_transicion.setText(transicion + ": " + String.format("%.1f", tra)+"%");
                dialog_saque.setText(saque + ": " + String.format("%.1f", saq)+"%");
                dialog_posicion.setText(position + ": " + String.format("%.1f", pos)+"%");

                titulo_dialogo.setTypeface(tf);
                dialog_ataque.setTypeface(tf);
                dialog_defensa.setTypeface(tf);
                dialog_transicion.setTypeface(tf);
                dialog_saque.setTypeface(tf);
                dialog_posicion.setTypeface(tf);

                //Boton aceptar
                Button btn_dialogo_historial_ok = (Button) dialogo_historial.findViewById(R.id.btn_dialogo_historial_ok);
                btn_dialogo_historial_ok.setTypeface(tf);
                btn_dialogo_historial_ok.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialogo_historial.dismiss();
                    }
                });

                dialogo_historial.show();

                //Mostramos la view radarchart
                mostrarGrafico();

                ObjectAnimator anim_ata = ObjectAnimator.ofInt(dialog_progressBarAtaque, "progress", 0, Math.round(ata));
                anim_ata.setDuration(2000);
                anim_ata.setInterpolator(new DecelerateInterpolator());
                anim_ata.start();
                ObjectAnimator anim_def = ObjectAnimator.ofInt(dialog_progressBarDefensa, "progress", 0, Math.round(def));
                anim_def.setDuration(2000);
                anim_def.setInterpolator(new DecelerateInterpolator());
                anim_def.start();
                ObjectAnimator anim_tra = ObjectAnimator.ofInt(dialog_progressBarTransicion, "progress", 0, Math.round(tra));
                anim_tra.setDuration(2000);
                anim_tra.setInterpolator(new DecelerateInterpolator());
                anim_tra.start();
                ObjectAnimator anim_saq = ObjectAnimator.ofInt(dialog_progressBarSaque, "progress", 0, Math.round(saq));
                anim_saq.setDuration(2000);
                anim_saq.setInterpolator(new DecelerateInterpolator());
                anim_saq.start();
                ObjectAnimator anim_pos = ObjectAnimator.ofInt(dialog_progressBarPosicion, "progress", 0, Math.round(pos));
                anim_pos.setDuration(2000);
                anim_pos.setInterpolator(new DecelerateInterpolator());
                anim_pos.start();

            }
        });
    }

    private void mostrarGrafico() {
        //==========================================================================
        // Añadimos el gráfico de Rubén RadarChart
        //==========================================================================
        grafico_dialogo.setDescription(null);
        grafico_dialogo.setWebLineWidth(2f);
        //Lineas desde el centro
        grafico_dialogo.setWebColor(getResources().getColor(R.color.colorBlanco));
        grafico_dialogo.setWebLineWidthInner(2f);
        //Lineas desde vertices
        grafico_dialogo.setWebColorInner(getResources().getColor(R.color.colorBlanco));
        grafico_dialogo.setWebAlpha(100);
        // Marker customizado
        MarkerView mv = new RadarMarkerView(this, R.layout.radar_marker);
        mv.setChartView(grafico_dialogo);
        grafico_dialogo.setMarker(mv);
        grafico_dialogo.animateY(1400);

        //*************************************************************************
        //DATOS DEL RADARCHART
        //*************************************************************************
        ArrayList<RadarEntry> arrayDatosEjeY = new ArrayList<RadarEntry>();

        float[] golpes_datos = {ata,def,saq,pos,tra};
        for (int i = 0; i < golpes_datos.length; i++)
            arrayDatosEjeY.add(new RadarEntry(golpes_datos[i], i));

        RadarDataSet datosY = new RadarDataSet(arrayDatosEjeY, "Golpes");
        switch (res) {
            case 1:
                //Contorno del radar
                datosY.setColors(getResources().getColor(R.color.azul_claro));
                //Interior del radar
                datosY.setFillColor(getResources().getColor(R.color.azul_claro));
                break;
            case 2:
                //Contorno del radar
                datosY.setColors(getResources().getColor(R.color.rojo_claro));
                //Interior del radar
                datosY.setFillColor(getResources().getColor(R.color.rojo_claro));
                break;
            default:
                //Contorno del radar
                datosY.setColors(getResources().getColor(R.color.azul_claro));
                //Interior del radar
                datosY.setFillColor(getResources().getColor(R.color.azul_claro));
                break;
        }
        datosY.setDrawFilled(true);
        //Transparencia dentro del radar, 0 es totalmente transparente
        datosY.setFillAlpha(150);
        //Grosor del contorno del radar
        datosY.setLineWidth(2f);
        datosY.setDrawHighlightCircleEnabled(true);
        datosY.setDrawHighlightIndicators(false);

        RadarData datos = new RadarData(datosY);
        datos.setValueFormatter(new PercentFormatter());
        //Tamaño texto dentro de radar
        datos.setValueTextSize(15f);
        //Color texto dentro de radar
        datos.setValueTextColor(getResources().getColor(R.color.colorPrimary));
        datos.setValueTypeface(tf);
        datos.setDrawValues(false);

        //Valores dentro del radar
        YAxis yAxis = grafico_dialogo.getYAxis();
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(100f);
        yAxis.setLabelCount(6, true);
        yAxis.setDrawLabels(true);
        yAxis.setTypeface(tf);
        yAxis.setTextColor(getResources().getColor(R.color.colorPrimary));

        //Nombre fuera del radar
        XAxis xAxis = grafico_dialogo.getXAxis();
        xAxis.setTypeface(tf);
        xAxis.setTextSize(12f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setTextColor(getResources().getColor(R.color.colorPrimary));
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return golpes_nombres[(int) value % golpes_nombres.length];
            }
            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });

        grafico_dialogo.setData(datos);
        //Quitar seleccionados
        grafico_dialogo.highlightValues(null);
        //Actualiza gráfico
        grafico_dialogo.invalidate();

        Legend leyenda = grafico_dialogo.getLegend();
        leyenda.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        leyenda.setXEntrySpace(7);
        leyenda.setYEntrySpace(5);
        //Ocultar leyenda
        leyenda.setEnabled(false);

    }
}

