package applepiedev.com.padelstats.Drawer.Statistics;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;

import applepiedev.com.padelstats.BBDD.SQLDataBase;
import applepiedev.com.padelstats.Models.AtaqueModel;
import applepiedev.com.padelstats.R;
import applepiedev.com.padelstats.RadarMarkerView;

/**
 * Created by Ruben on 11/12/2016.
 */

public class Estadisticas extends AppCompatActivity implements OnChartGestureListener, OnChartValueSelectedListener {

    private LineChart grafico;
    private Typeface tf;
    private SQLiteDatabase db;
    private String count = "SELECT count(*) FROM Estadisticas";
    private Float ata, def, tra, pos, saq;
    private ArrayList<Float> ataques = new ArrayList<Float>();
    private ArrayList<Float> defensas = new ArrayList<Float>();
    private ArrayList<Float> transiciones = new ArrayList<Float>();
    private ArrayList<Float> saques = new ArrayList<Float>();
    private ArrayList<Float> posiciones = new ArrayList<Float>();
    private ArrayList<Float> globales = new ArrayList<Float>();
    private ArrayList<ILineDataSet> dataSets;
    private AtaqueModel ataqueModel;
    private AtaqueModel defensaModel;
    private AtaqueModel transicionModel;
    private AtaqueModel saqueModel;
    private AtaqueModel posicionModel;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //handle the home button onClick event here.
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.estadisticas);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        grafico = (LineChart) findViewById(R.id.grafico);
        tf = Typeface.createFromAsset(getAssets(), "bubblerone.ttf");
        dataSets = new ArrayList<ILineDataSet>();

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        ataqueModel= (AtaqueModel) bundle.getSerializable("ataque");
        defensaModel= (AtaqueModel) bundle.getSerializable("defensa");
        transicionModel= (AtaqueModel) bundle.getSerializable("transicion");
        saqueModel= (AtaqueModel) bundle.getSerializable("saque");
        posicionModel= (AtaqueModel) bundle.getSerializable("posicion");
        //==========================================================================
        // Consultamos si existen registros en la tabla de la BBDD
        //==========================================================================
        //Abrimos la base de datos 'DBEstadisticas' en modo escritura
        SQLDataBase sqldb = new SQLDataBase(this, "DBEstadisticas", null, 1);
        db = sqldb.getWritableDatabase();

        //Comprobamos si existen registros dentro de la tabla
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        //Si la base de datos no tiene registros
        if (icount == 0) {
            //Cambiar visibilidades de las views
        }

        //Si la base de datos si tiene registros
        else {
            //Muestro y oculto views
            //método rawQuery()
            Cursor c = db.rawQuery("SELECT id, ataque, defensa, transicion, saque, posicion, resultado, day, month, year, time FROM Estadisticas", null);
            //Recorremos los resultados para mostrarlos en pantalla
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya más registros
                do {
                    //Cogemos los datos del registro y los guardamos en variables
                    int id = c.getInt(0);
                    float ata = c.getFloat(1);
                    float def = c.getFloat(2);
                    float tra = c.getFloat(3);
                    float saq = c.getFloat(4);
                    float pos = c.getFloat(5);
                    int res = c.getInt(6);
                    int day = c.getInt(7);
                    int month = c.getInt(8);
                    int year = c.getInt(9);
                    String time = c.getString(10);

                    //Seteo de datos en arrays
                    ataques.add(ata);
                    defensas.add(def);
                    transiciones.add(tra);
                    saques.add(saq);
                    posiciones.add(pos);
                    //globales.add((ata + def + tra + saq + pos) / 5);

                } while (c.moveToNext());
            }
            //grafico.setOnChartGestureListener(this);
            //grafico.setOnChartValueSelectedListener(this);
            //grafico.setDrawGridBackground(false);

            // no description text
            grafico.setDescription(null);

            // enable / disable grid background
            grafico.setDrawGridBackground(false);

            // enable touch gestures
            grafico.setTouchEnabled(true);

            // enable scaling and dragging
            grafico.setDragEnabled(true);
            grafico.setScaleEnabled(true);
            // mChart.setScaleXEnabled(true);
            // mChart.setScaleYEnabled(true);

            // if disabled, scaling can be done on x- and y-axis separately
            grafico.setPinchZoom(true);

            // set custom chart offsets (automatic offset calculation is hereby disabled)
            //grafico.setViewPortOffsets(10, 0, 10, 0);

            // set an alternative background color
            // mChart.setBackgroundColor(Color.GRAY);

            // create a custom MarkerView (extend MarkerView) and specify the layout
            MarkerView mv = new RadarMarkerView(this, R.layout.radar_marker);
            mv.setChartView(grafico);
            grafico.setMarker(mv);

            // x-axis limit line
        /*LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setLineWidth(4f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);*/

            XAxis xAxis = grafico.getXAxis();
            xAxis.setTextColor(R.color.colorGris);
            //xAxis.setGridColor(Color.TRANSPARENT);
            xAxis.setEnabled(false);
            //xAxis.enableGridDashedLine(10f, 10f, 0f);
            //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
            //xAxis.addLimitLine(llXAxis); // add x-axis limit line


       /* LimitLine ll1 = new LimitLine(150f, "Upper Limit");
        ll1.setLineWidth(4f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll1.setTextSize(10f);*/
            //ll1.setTypeface(tf);

        /*LimitLine ll2 = new LimitLine(-30f, "Lower Limit");
        ll2.setLineWidth(4f);
        ll2.enableDashedLine(10f, 10f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);*/
            //ll2.setTypeface(tf);

            YAxis leftAxis = grafico.getAxisLeft();
            leftAxis.setTextColor(R.color.colorGris);
            //leftAxis.setGridColor(Color.TRANSPARENT);
            leftAxis.setEnabled(true);
            //leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
            //leftAxis.addLimitLine(ll1);
            //leftAxis.addLimitLine(ll2);
            //leftAxis.setAxisMaxValue(100f);
            //leftAxis.setAxisMinValue(0f);
            //leftAxis.setYOffset(20f);
            //leftAxis.enableGridDashedLine(10f, 10f, 0f);
            //leftAxis.setDrawZeroLine(false);

            // limit lines are drawn behind data (and not on top)
            //leftAxis.setDrawLimitLinesBehindData(true);

            grafico.getAxisRight().setEnabled(false);

            //mChart.getViewPortHandler().setMaximumScaleY(2f);
            //mChart.getViewPortHandler().setMaximumScaleX(2f);

            // add data
            introducirDatos();
            ataque();
            defensa();
            setTransiciones();
            saque();
            posicion();

            LineData data = new LineData(dataSets);

            grafico.animateY(1000, Easing.EasingOption.EaseInOutCubic);
            //mChart.invalidate();

            // get the legend (only possible after setting data)
            Legend l = grafico.getLegend();
            int colorcodes[] = l.getColors();

            // modify the legend ...
            //l.setPosition(Legend.LegendPosition.ABOVE_CHART_CENTER);
            l.setForm(Legend.LegendForm.CIRCLE);
            grafico.getAxisLeft().setTextColor(Color.WHITE);
            grafico.getXAxis().setTextColor(Color.WHITE);
            l.setEnabled(true);
            grafico.getLegend().setTextColor(Color.WHITE);
            l.setTextSize(12);
            // // dont forget to refresh the drawing
            grafico.setData(data);
            grafico.invalidate();

//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);


        }
    }

    private void introducirDatos() {

        ArrayList<Entry> values = new ArrayList<Entry>();

        for (int i=0; i<ataqueModel.getAtacks().size();i++){
            globales.add((ataqueModel.getAtacks().get(i)
                    + defensaModel.getAtacks().get(i)
                    + transicionModel.getAtacks().get(i)
                    + saqueModel.getAtacks().get(i)
                    + posicionModel.getAtacks().get(i)) / 5);
        }

        Float[] global = new Float[globales.size()];
        global = globales.toArray(global);

        for (int i = 0; i < global.length; i++)
            values.add(new Entry(i, global[i]));

        LineDataSet datosY;
        datosY = new LineDataSet(values, "Global");

        // Tipo de linea punteada
        //datosY.enableDashedLine(10f, 5f, 0f);
        //datosY.enableDashedHighlightLine(10f, 5f, 0f);
        datosY.setColor(getResources().getColor(R.color.naranja_claro));
        datosY.setColor(getResources().getColor(R.color.naranja_claro));
        //Anchura linea
        datosY.setLineWidth(1f);
        //Circulo externo
        datosY.setCircleColor(getResources().getColor(R.color.naranja_claro));
        datosY.setCircleRadius(5f);
        //Circulo interno
        datosY.setDrawCircleHole(true);
        datosY.setCircleHoleRadius(2f);
        datosY.setCircleColorHole(getResources().getColor(R.color.verde_claro));
        //Tamaño texto en el grafico
        datosY.setValueTextSize(10f);
        datosY.setValueTextColor(getResources().getColor(R.color.verde_claro));
        datosY.setValueTypeface(tf);
        //Degradado dentro del grafico
        datosY.setDrawFilled(false);
        //Modo curva
        datosY.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.estadisticas_degradado);
            datosY.setFillDrawable(drawable);
        } else {
            datosY.setFillColor(getResources().getColor(R.color.colorGris));
        }

        /*ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);*/ // add the datasets
        dataSets.add(datosY);
        //grafico.setData(dataSets);
    }

    private void ataque() {

        ArrayList<Entry> valuesAtaque = new ArrayList<Entry>();

        Float[] ataque = new Float[ataqueModel.getAtacks().size()];
        ataque = ataqueModel.getAtacks().toArray(ataque);

        for (int i = 0; i < ataque.length; i++)
            valuesAtaque.add(new Entry(i, ataque[i])
            );
        LineDataSet ataquesY = new LineDataSet(valuesAtaque, "Ataque");
        // Tipo de linea punteada
        //datosY.enableDashedLine(10f, 5f, 0f);
        //datosY.enableDashedHighlightLine(10f, 5f, 0f);
        ataquesY.setColor(getResources().getColor(R.color.verde_claro));
        //Anchura linea
        ataquesY.setLineWidth(1f);
        //Circulo externo
        ataquesY.setCircleColor(

                getResources().getColor(R.color.verde_claro)

        );
        ataquesY.setCircleRadius(5f);
        //Circulo interno
        ataquesY.setDrawCircleHole(true);
        ataquesY.setCircleHoleRadius(2f);
        ataquesY.setCircleColorHole(getResources().getColor(R.color.naranja_claro));
        //Tamaño texto en el grafico
        ataquesY.setValueTextSize(10f);
        ataquesY.setValueTextColor(getResources().getColor(R.color.naranja_claro));
        ataquesY.setValueTypeface(tf);
        //Degradado dentro del grafico
        ataquesY.setDrawFilled(false);
        //Modo curva
        ataquesY.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.estadisticas_degradado);
            ataquesY.setFillDrawable(drawable);
        }else {
            ataquesY.setFillColor(getResources().getColor(R.color.colorGris));
        }

        /*ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);*/ // add the datasets

        dataSets.add(ataquesY);
    }

    private void defensa() {

        ArrayList<Entry> valuesDefensa = new ArrayList<Entry>();

        Float[] defensa = new Float[defensaModel.getAtacks().size()];
        defensa = defensaModel.getAtacks().toArray(defensa);

        for (int i = 0; i < defensa.length; i++)
            valuesDefensa.add(new Entry(i, defensa[i])
            );
        LineDataSet defensasY = new LineDataSet(valuesDefensa, "Defensa");
        // Tipo de linea punteada
        //datosY.enableDashedLine(10f, 5f, 0f);
        //datosY.enableDashedHighlightLine(10f, 5f, 0f);
        defensasY.setColor(getResources().getColor(R.color.rojo_claro));
        //Anchura linea
        defensasY.setLineWidth(1f);
        //Circulo externo
        defensasY.setCircleColor(getResources().getColor(R.color.rojo_claro));
        defensasY.setCircleRadius(5f);
        //Circulo interno
        defensasY.setDrawCircleHole(true);
        defensasY.setCircleHoleRadius(2f);
        defensasY.setCircleColorHole(getResources().getColor(R.color.azul_claro));
        //Tamaño texto en el grafico
        defensasY.setValueTextSize(10f);
        defensasY.setValueTextColor(getResources().getColor(R.color.azul_claro));
        defensasY.setValueTypeface(tf);
        //Degradado dentro del grafico
        defensasY.setDrawFilled(false);
        //Modo curva
        defensasY.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.estadisticas_degradado);
            defensasY.setFillDrawable(drawable);
        }else {
            defensasY.setFillColor(getResources().getColor(R.color.colorGris));
        }

        /*ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);*/ // add the datasets

        dataSets.add(defensasY);
    }

    private void setTransiciones() {

        ArrayList<Entry> valuesDTrans = new ArrayList<Entry>();

        Float[] transicion = new Float[transicionModel.getAtacks().size()];
        transicion = transicionModel.getAtacks().toArray(transicion);

        for (int i = 0; i < transicion.length; i++)
            valuesDTrans.add(new Entry(i, transicion[i])
            );
        LineDataSet transicionY = new LineDataSet(valuesDTrans, "Transición");
        // Tipo de linea punteada
        //datosY.enableDashedLine(10f, 5f, 0f);
        //datosY.enableDashedHighlightLine(10f, 5f, 0f);
        transicionY.setColor(getResources().getColor(R.color.welcome_amarillo));
        //Anchura linea
        transicionY.setLineWidth(1f);
        //Circulo externo
        transicionY.setCircleColor(getResources().getColor(R.color.welcome_amarillo));
        transicionY.setCircleRadius(5f);
        //Circulo interno
        transicionY.setDrawCircleHole(true);
        transicionY.setCircleHoleRadius(2f);
        transicionY.setCircleColorHole(getResources().getColor(R.color.welcome_naranja));
        //Tamaño texto en el grafico
        transicionY.setValueTextSize(10f);
        transicionY.setValueTextColor(getResources().getColor(R.color.welcome_naranja));
        transicionY.setValueTypeface(tf);
        //Degradado dentro del grafico
        transicionY.setDrawFilled(false);
        //Modo curva
        transicionY.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.estadisticas_degradado);
            transicionY.setFillDrawable(drawable);
        }else {
            transicionY.setFillColor(getResources().getColor(R.color.colorGris));
        }

        /*ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);*/ // add the datasets

        dataSets.add(transicionY);
    }

    private void saque() {

        ArrayList<Entry> valuesSaque = new ArrayList<Entry>();

        Float[] saque = new Float[saqueModel.getAtacks().size()];
        saque = saqueModel.getAtacks().toArray(saque);

        for (int i = 0; i < saque.length; i++)
            valuesSaque.add(new Entry(i, saque[i])
            );
        LineDataSet saqueY = new LineDataSet(valuesSaque, "Transición");
        // Tipo de linea punteada
        //datosY.enableDashedLine(10f, 5f, 0f);
        //datosY.enableDashedHighlightLine(10f, 5f, 0f);
        saqueY.setColor(getResources().getColor(R.color.purple_background));
        //Anchura linea
        saqueY.setLineWidth(1f);
        //Circulo externo
        saqueY.setCircleColor(getResources().getColor(R.color.purple_background));
        saqueY.setCircleRadius(5f);
        //Circulo interno
        saqueY.setDrawCircleHole(true);
        saqueY.setCircleHoleRadius(2f);
        saqueY.setCircleColorHole(getResources().getColor(R.color.piechart_azul));
        //Tamaño texto en el grafico
        saqueY.setValueTextSize(10f);
        saqueY.setValueTextColor(getResources().getColor(R.color.piechart_azul));
        saqueY.setValueTypeface(tf);
        //Degradado dentro del grafico
        saqueY.setDrawFilled(false);
        //Modo curva
        saqueY.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.estadisticas_degradado);
            saqueY.setFillDrawable(drawable);
        }else {
            saqueY.setFillColor(getResources().getColor(R.color.colorGris));
        }

        /*ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);*/ // add the datasets

        dataSets.add(saqueY);
    }

    private void posicion() {

        ArrayList<Entry> valuesPosicion = new ArrayList<Entry>();

        Float[] posicion = new Float[posicionModel.getAtacks().size()];
        posicion = posicionModel.getAtacks().toArray(posicion);

        for (int i = 0; i < posicion.length; i++)
            valuesPosicion.add(new Entry(i, posicion[i])
            );
        LineDataSet posicionY = new LineDataSet(valuesPosicion, "Transición");
        // Tipo de linea punteada
        //datosY.enableDashedLine(10f, 5f, 0f);
        //datosY.enableDashedHighlightLine(10f, 5f, 0f);
        posicionY.setColor(getResources().getColor(R.color.teal_background));
        //Anchura linea
        posicionY.setLineWidth(1f);
        //Circulo externo
        posicionY.setCircleColor(getResources().getColor(R.color.teal_background));
        posicionY.setCircleRadius(5f);
        //Circulo interno
        posicionY.setDrawCircleHole(true);
        posicionY.setCircleHoleRadius(2f);
        posicionY.setCircleColorHole(getResources().getColor(R.color.azul_oscuro));
        //Tamaño texto en el grafico
        posicionY.setValueTextSize(10f);
        posicionY.setValueTextColor(getResources().getColor(R.color.azul_oscuro));
        posicionY.setValueTypeface(tf);
        //Degradado dentro del grafico
        posicionY.setDrawFilled(false);
        //Modo curva
        posicionY.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.estadisticas_degradado);
            posicionY.setFillDrawable(drawable);
        }else {
            posicionY.setFillColor(getResources().getColor(R.color.colorGris));
        }

        /*ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);*/ // add the datasets

        dataSets.add(posicionY);
    }


    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
