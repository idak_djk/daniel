package applepiedev.com.padelstats.Main.Implementations;

import android.util.Log;

import java.util.List;

import applepiedev.com.padelstats.Main.Interfaces.MainActivityRepository;
import applepiedev.com.padelstats.Main.events.MainActivityEvents;
import applepiedev.com.padelstats.Models.GlobalsModel;
import applepiedev.com.padelstats.lib.eventbus.Eventbus;
import applepiedev.com.padelstats.lib.eventbus.GreenRobotEventbus;
import applepiedev.com.padelstats.lib.retrofit.ApiCallback;
import applepiedev.com.padelstats.lib.retrofit.ApiData;
import applepiedev.com.padelstats.lib.retrofit.AppClient;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniel on 24/02/2017.
 */

public class MainActivityRepositoryImpl implements MainActivityRepository {
    public ApiData apiData = AppClient.retrofit().create(ApiData.class);
    private CompositeSubscription mCompositeSubscription;

    @Override
    public void getDataUser(String userId) {
        addSubscription(apiData.getData("getData",userId),
                new ApiCallback<List<GlobalsModel>>() {
                    @Override
                    public void onSuccess(List<GlobalsModel> model) {
                        postEvent(MainActivityEvents.ongetDataSuccess, model);
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e("ERROR", error);
                        postEvent(MainActivityEvents.ongetDataError);
                    }

                    @Override
                    public void onFinish() {
                        // mvpView.hideLoading();
                    }
                });
    }

    public void addSubscription(Observable observable, Subscriber subscriber) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        mCompositeSubscription.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber));
    }

    // =========== Eventbus Post Events Methods ================

    private void postEvent(int type, String error) {
        MainActivityEvents mainActivityEvents = new MainActivityEvents();
        mainActivityEvents.setEventType(type);
        if (error != null) {
            mainActivityEvents.setErrorMessage(error);
        }
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(mainActivityEvents);
    }

    private void postEvent(int type, List<GlobalsModel> model){
        MainActivityEvents mainActivityEvents = new MainActivityEvents();
        mainActivityEvents.setEventType(type);
        mainActivityEvents.setGlobalModel(model);
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(mainActivityEvents);
    }

    private void postEvent(int type) {
        postEvent(type);
    }

    @Override
    public void getDataUserSuccess() {

    }

    @Override
    public void getDataUserError() {

    }
}
