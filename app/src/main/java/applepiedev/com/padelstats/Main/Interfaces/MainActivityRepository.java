package applepiedev.com.padelstats.Main.Interfaces;

/**
 * Created by Daniel on 24/02/2017.
 */

public interface MainActivityRepository {
    // Obtención de datos
    void getDataUser(String userId);
    void getDataUserSuccess();
    void getDataUserError();
}
