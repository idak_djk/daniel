package applepiedev.com.padelstats.Main;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.Base64;
import android.view.Display;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.fabtransitionactivity.SheetLayout;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.stephentuso.welcome.WelcomeScreenHelper;

import java.util.ArrayList;
import java.util.List;

import applepiedev.com.padelstats.BBDD.SQLDataBase;
import applepiedev.com.padelstats.Circulo.CircleProgressBar;
import applepiedev.com.padelstats.Drawer.History.Historial;
import applepiedev.com.padelstats.Drawer.Statistics.Estadisticas;
import applepiedev.com.padelstats.Login.LoginActivity;
import applepiedev.com.padelstats.Main.Implementations.MainActivityPresenterImpl;
import applepiedev.com.padelstats.Main.Interfaces.MainActivityView;
import applepiedev.com.padelstats.Models.AtaqueModel;
import applepiedev.com.padelstats.Models.GlobalsModel;
import applepiedev.com.padelstats.Models.LoginModel;
import applepiedev.com.padelstats.Profile.NewScrollingActivity;
import applepiedev.com.padelstats.R;
import applepiedev.com.padelstats.RadarMarkerView;
import applepiedev.com.padelstats.Stepper.StepperActivity;
import applepiedev.com.padelstats.Wellcome.MyWelcomeActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SheetLayout.OnFabAnimationEndListener,
        MainActivityView {

    private String[] golpes_nombres;
    private RadarChart grafico;
    private WelcomeScreenHelper welcomeScreen;
    private SheetLayout mSheetLayout;
    private Typeface tf;
    private static final int REQUEST_CODE = 1;
    public SharedPreferences prefs;
    private SQLiteDatabase db;
    private String count = "SELECT count(*) FROM Estadisticas";
    private TextView texto_main_sin_datos, text_winner, text_loser;
    private Float suma_ataque = 0.0f;
    private Float suma_defensa = 0.0f;
    private Float suma_transicion = 0.0f;
    private Float suma_saque = 0.0f;
    private Float suma_posicion = 0.0f;
    private int suma_numero_registros = 0;
    private int suma_partidos_ganados = 0;
    private int suma_partidos_perdidos = 0;
    private CircleProgressBar circulo_ganado, circulo_perdido;
    private FrameLayout lyt_circulo_ganado;
    private LinearLayout lyt_circulo_perdido;
    private int pulsado_ganado = 0;
    private int pulsado_perdido =0;
    private Bitmap image;
    private SharedPreferences.Editor editor;
    private CircleImageView imageprofile;
    private NavigationView navigationView;
    private View hView;
    private SharedPreferences myPrefrence;
    private TextView tv_nameprofile;
    private TextView tv_skillProfile;
    private TextView tv_percentProfile;
    private CircleProgressBar circleProfile;
    private MainActivityPresenterImpl mainActivityPresenter;
    private float ataques;
    private ArrayList atack,def,tran,saq,pos;
    private AtaqueModel ataqueModel,defensaModel,transicionModel,saqueModel,posicionModel;
    private AtaqueModel resultadoModel;
    private ArrayList resu;
    private String userId;
    private String userName;
    private String userImage;
    private LoginModel loginModel;
    private String userLevel;
    private Bundle extras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Controla excepciones al volver a la actividad principal
        // Get Extras
        extras = getIntent().getExtras();
        try {
            loginModel = (LoginModel) extras.get("LoginModel");
            userId = loginModel.getId_user();
            userName = loginModel.getName_user();
            userImage = loginModel.getUrl_pic_user();
            userLevel = loginModel.getLevel_user();
        }catch (NullPointerException e){
            e.printStackTrace();
        }



        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        TextView main_title = (TextView) findViewById(R.id.main_title);
        TextView tv_ganados = (TextView) findViewById(R.id.tv_ganados);
        TextView tv_perdidos = (TextView) findViewById(R.id.tv_perdidos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        grafico = (RadarChart) findViewById(R.id.grafico);
        mSheetLayout = (SheetLayout) findViewById(R.id.bottom_sheet);
        texto_main_sin_datos = (TextView)findViewById(R.id.texto_main_sin_datos);
        circulo_ganado = (CircleProgressBar) findViewById(R.id.circulo1);
        circulo_perdido = (CircleProgressBar) findViewById(R.id.circulo2);
        text_winner = (TextView) findViewById(R.id.text_winner);
        text_loser = (TextView) findViewById(R.id.text_loser);
        lyt_circulo_ganado = (FrameLayout) findViewById(R.id.lyt_circulo_ganado);
        lyt_circulo_perdido = (LinearLayout) findViewById(R.id.lyt_circulo_perdido);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        hView =  navigationView.getHeaderView(0);
        imageprofile = (CircleImageView) hView.findViewById(R.id.imageprofileheader);
        tv_nameprofile = (TextView) hView.findViewById(R.id.tv_nameProfile);
        tv_skillProfile = (TextView) hView.findViewById(R.id.tv_skillProfile);
        tv_percentProfile = (TextView) hView.findViewById(R.id.tv_percentProfile);
        circleProfile = (CircleProgressBar) hView.findViewById(R.id.circleProfile);
        golpes_nombres = new String[]{getString(R.string.ataque), getString(R.string.defensa),
                getString(R.string.saque), getString(R.string.posicion), getString(R.string.transicion)};
        setSupportActionBar(toolbar);

        myPrefrence = PreferenceManager
                .getDefaultSharedPreferences(this);
        editor = myPrefrence.edit();
        String rEmail = myPrefrence.getString("email",null);

        // Presenter
        mainActivityPresenter = new MainActivityPresenterImpl(this, MainActivity.this);
        mainActivityPresenter.onCreate();


        tf = Typeface.createFromAsset(getAssets(), "questrial.ttf");
        main_title.setTypeface(tf);
        tv_ganados.setTypeface(tf);
        tv_perdidos.setTypeface(tf);
        //main_title2.setTypeface(tf);

        //Inicio animacion de la view fab
        fab.setScaleX(0);
        fab.setScaleY(0);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            final Interpolator interpolador = AnimationUtils.loadInterpolator(getBaseContext(),
                    android.R.interpolator.fast_out_slow_in);

            fab.animate()
                    .scaleX(1)
                    .scaleY(1)
                    .setInterpolator(interpolador)
                    .setDuration(600)
                    .setStartDelay(1000)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }
                        @Override
                        public void onAnimationEnd(Animator animation) {
                        }
                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }
                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        }
        //Final animacion de la view fab

        welcomeScreen = new WelcomeScreenHelper(this, MyWelcomeActivity.class);
        welcomeScreen.show(savedInstanceState);

        //==========================================================================
        // Listener del FAB
        //==========================================================================
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSheetLayout.expandFab();
            }
        });

        mSheetLayout.setFab(fab);
        mSheetLayout.setFabAnimationEndListener(this);

        //==========================================================================
        // Navigation drawer
        //==========================================================================
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
/*
        if(myPrefrence.getString("imageProfile",null) != null) {
            Bitmap rImageProfile = decodeBase64(myPrefrence.getString("imageProfile",null));
            try {
                imageprofile.setImageBitmap(rImageProfile);
            }catch (Exception e){
                imageprofile.setImageResource(R.drawable.profiledani);
            }
        }else{
            imageprofile.setImageResource(R.drawable.profiledani);
        }
        */

        //==========================================================================
        // Consultamos si existen registros en la tabla de la BBDD
        //==========================================================================
        //Abrimos la base de datos 'DBEstadisticas' en modo escritura
        SQLDataBase sqldb = new SQLDataBase(this, "DBEstadisticas", null, 1);
        db = sqldb.getWritableDatabase();

        //Comprobamos si existen registros dentro de la tabla
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        //Si la base de datos no tiene registros

        if(icount==0){
            grafico.setVisibility(View.GONE);
           // circulo_ganado.setVisibility(View.GONE);
           // circulo_perdido.setVisibility(View.GONE);
            text_loser.setVisibility(View.GONE);
            text_winner.setVisibility(View.GONE);
            texto_main_sin_datos.setVisibility(View.VISIBLE);
        }
        //Si la base de datos si tiene registros
        else{
            //Muestro y oculto views
            grafico.setVisibility(View.VISIBLE);
            texto_main_sin_datos.setVisibility(View.GONE);
            //Inicvializo las variables donde guardo las sumas de los datos
            suma_ataque=0.0f;
            suma_defensa=0.0f;
            suma_transicion=0.0f;
            suma_saque=0.0f;
            suma_posicion=0.0f;
            suma_numero_registros=0;
            suma_partidos_ganados=0;
            suma_partidos_perdidos=0;
            //método rawQuery()
            Cursor c = db.rawQuery("SELECT ataque, defensa, transicion, saque, posicion, resultado FROM Estadisticas", null);
            //Recorremos los resultados para mostrarlos en pantalla
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya más registros
                do {
                    float ata = c.getFloat(0);
                    float def = c.getFloat(1);
                    float tra = c.getFloat(2);
                    float saq = c.getFloat(3);
                    float pos = c.getFloat(4);
                    int res = c.getInt(5);
                    //Guardamos variables
                    suma_ataque = suma_ataque + ata;
                    suma_defensa = suma_defensa + def;
                    suma_transicion = suma_transicion + tra;
                    suma_saque = suma_saque + saq;
                    suma_posicion = suma_posicion + pos;
                    //suma_numero_registros++;
                    /*if(res == 1){
                        suma_partidos_ganados++;
                    }
                    else{
                        suma_partidos_perdidos++;
                    }*/

                } while(c.moveToNext());
            }
            //Hacemos la media de cada campo dependiendo del numero de registrso existentes en la BBDD
            suma_ataque = suma_ataque/suma_numero_registros;
            suma_defensa = suma_defensa/suma_numero_registros;
            suma_transicion = suma_transicion/suma_numero_registros;
            suma_saque = suma_saque/suma_numero_registros;
            suma_posicion = suma_posicion/suma_numero_registros;


        }

        //==========================================================================
        // Añadimos el gráfico de Rubén RadarChart
        //==========================================================================

        grafico.setDescription(null);
        grafico.setWebLineWidth(2f);
        //Lineas desde el centro
        grafico.setWebColor(getResources().getColor(R.color.colorEjes));
        grafico.setWebLineWidthInner(2f);
        //Lineas desde vertices
        grafico.setWebColorInner(getResources().getColor(R.color.colorEjes));
        grafico.setWebAlpha(100);
        // Marker customizado
        MarkerView mv = new RadarMarkerView(this, R.layout.radar_marker);
        mv.setChartView(grafico);
        grafico.setMarker(mv);
        grafico.animateY(1400, Easing.EasingOption.EaseInOutCubic);

        //*************************************************************************
        //Mostrar tutorial de inicio si es la primera vez que la app corre
        //*************************************************************************
        prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        if (prefs.getString("Primeravez", "si") == "si") {
            mostartTutorial();
        }

        //*************************************************************************
        //Introduccion de datos en el piechart
        //*************************************************************************

        //*************************************************************************
        //Leyenda
        //*************************************************************************
        Legend leyenda = grafico.getLegend();
        //Legend leyenda2 = grafico2.getLegend();
        leyenda.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        //leyenda2.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        leyenda.setXEntrySpace(7);
        leyenda.setYEntrySpace(5);
        // leyenda2.setXEntrySpace(7);
        // leyenda2.setYEntrySpace(5);
        //Ocultar leyenda
        leyenda.setEnabled(false);
        // leyenda2.setEnabled(false);
        getDataUser(userId);
    }

    private void dataProfile() {
        try {
            /*if (myPrefrence.getString("imageProfile", null) != null) {
                Bitmap rImageProfile = decodeBase64(myPrefrence.getString("imageProfile", null));
                imageprofile.setImageBitmap(rImageProfile);
            }*/
            Glide.with(this).load(userImage).into(imageprofile);
        }catch (Exception e){
            e.printStackTrace();
        }
        tv_percentProfile.setText("85");
        try {
            tv_skillProfile.setText(loginModel.getLevel_user());
        }catch (Exception e){
            e.printStackTrace();
        }
        Float percentage = Float.valueOf(tv_percentProfile.getText().toString());
        circleProfile.setProgress(percentage);
        if(percentage<=50){
            circleProfile.setColor(Color.YELLOW);
        tv_percentProfile.setBackground(getDrawable(R.drawable.cirlce_percent_background_yellow));
        }
        if(percentage>50) {
            circleProfile.setColor(Color.GREEN);
            tv_percentProfile.setBackground(getDrawable(R.drawable.cirlce_percent_background_green));
        }
        if(percentage>80) {
            circleProfile.setColor(Color.RED);
            tv_percentProfile.setBackground(getDrawable(R.drawable.cirlce_percent_background_red));
        }
        tv_nameprofile.setText(userName);
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        welcomeScreen.onSaveInstanceState(outState);
    }

    // =========================================================================


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        for(int i=0; i< menu.size(); i++){
            MenuItem itemMenu = menu.getItem(i);
            tintIcon(menu,i);
        }
        return true;
    }

    private void tintIcon(Menu menu, int i){
        Drawable icon = menu.getItem(i).getIcon();
        if(icon != null) {
            DrawableCompat.setTint(icon, ContextCompat.getColor(this, R.color.azul_oscuro));
            menu.getItem(i).setIcon(icon);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent i = new Intent(this, NewScrollingActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_statistics) {
            Intent i = new Intent(this, Estadisticas.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("ataque", ataqueModel);
            bundle.putSerializable("defensa", defensaModel);
            bundle.putSerializable("transicion", transicionModel);
            bundle.putSerializable("saque", saqueModel);
            bundle.putSerializable("posicion", posicionModel);
            i.putExtras(bundle);
            startActivity(i);

        } else if (id == R.id.nav_history) {
            Intent i = new Intent(this, Historial.class);
            startActivity(i);
        }else if (id == R.id.nav_logout) {
            Intent i = new Intent(this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    //****************************
    //Tutorial de inicio
    //****************************
    private void mostartTutorial() {

        String str_tutorial_1 = getResources().getString(R.string.str_tutorial_1);
        String str_tutorial_2 = getResources().getString(R.string.str_tutorial_2);
        String str_tutorial_3 = getResources().getString(R.string.str_tutorial_3);

        final Drawable img_estadistica = getResources().getDrawable(R.drawable.tutorial_estadisticas3, getTheme());
        final Drawable img_perfil = getResources().getDrawable(R.drawable.tutorial_perfil, getTheme());

        final Display pantalla = getWindowManager().getDefaultDisplay();

        final Rect posicion_pantalla_estadisticas = new Rect(0, 0, img_estadistica.getIntrinsicWidth(), img_estadistica.getIntrinsicHeight());
        posicion_pantalla_estadisticas.offset((pantalla.getWidth() / 2) - 150, (pantalla.getHeight() / 2));

        final Rect posicion_pantalla_toolbar = new Rect(0, 0, img_perfil.getIntrinsicWidth(), img_perfil.getIntrinsicHeight());
        posicion_pantalla_toolbar.offset(50, 50);

        final TapTargetSequence sequence = new TapTargetSequence(this)
                .targets(

                        TapTarget.forBounds(posicion_pantalla_estadisticas,
                                str_tutorial_1)
                                .tintTarget(false)
                                .textTypeface(tf)
                                .icon(img_estadistica)
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .targetCircleColor(R.color.colorBlanco)   // Specify a color for the target circle
                                .textColor(R.color.colorBlanco)            // Specify a color for text
                                .dimColor(R.color.colorNegro)            // If set, will dim behind the view with 30% opacity of the given color
                                .cancelable(false),

                        TapTarget.forView(findViewById(R.id.fab),
                                str_tutorial_2)
                                .tintTarget(false)
                                .textTypeface(tf)
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .targetCircleColor(R.color.colorBlanco)   // Specify a color for the target circle
                                .textColor(R.color.colorBlanco)            // Specify a color for text
                                .dimColor(R.color.colorNegro)            // If set, will dim behind the view with 30% opacity of the given color
                                .cancelable(false)
                                .drawShadow(true),

                        TapTarget.forBounds(posicion_pantalla_toolbar,
                                str_tutorial_3)
                                .tintTarget(false)
                                .textTypeface(tf)
                                .icon(img_perfil)
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .targetCircleColor(R.color.colorBlanco)   // Specify a color for the target circle
                                .textColor(R.color.colorBlanco)            // Specify a color for text
                                .dimColor(R.color.colorNegro)            // If set, will dim behind the view with 30% opacity of the given color
                                .cancelable(false)

                )
                .listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("Primeravez", "no");
                        editor.commit();
                    }

                    @Override
                    public void onSequenceCanceled() {

                    }
                });
        sequence.start();
    }

    // =========================================================================
    //ANIMACION FAB PARA PASAR A STEPPER
    // =========================================================================
    @Override
    public void onFabAnimationEnd() {
        Intent intent = new Intent(this, StepperActivity.class);
        intent.putExtra("USER_ID",userId);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            //finish();
        }
        if (requestCode == REQUEST_CODE) {
            mSheetLayout.contractFab();
        }
    }

    // method for base64 to bitmap
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    @Override

    protected void onResume() {
        dataProfile();
        super.onResume();
    }

    @Override
    public void getDataUser(String userId) {
        mainActivityPresenter.getDataUser(userId);
    }

    @Override
    public void getDataUserSuccess(List<GlobalsModel> model) {
        suma_ataque = 0.0f;
        suma_defensa = 0.0f;
        suma_transicion = 0.0f;
        suma_saque = 0.0f;
        suma_posicion = 0.0f;
        ataqueModel = new AtaqueModel();
        defensaModel = new AtaqueModel();
        transicionModel = new AtaqueModel();
        saqueModel = new AtaqueModel();
        posicionModel = new AtaqueModel();
        resultadoModel = new AtaqueModel();
        atack = new ArrayList();
        def = new ArrayList();
        tran = new ArrayList();
        saq = new ArrayList();
        pos = new ArrayList();
        resu = new ArrayList();
        for(int i =0; i<model.size();i++) {
            suma_numero_registros = i;
            atack.add(model.get(i).getAtaques());
            suma_ataque += model.get(i).getAtaques();
            def.add(model.get(i).getDefensas());
            suma_defensa += model.get(i).getDefensas();
            tran.add(model.get(i).getTransiciones());
            suma_transicion += model.get(i).getTransiciones();
            saq.add(model.get(i).getSaques());
            suma_saque += model.get(i).getSaques();
            pos.add(model.get(i).getPosicions());
            suma_posicion += model.get(i).getPosicions();
            resu.add(model.get(i).getResultados());
        }
        if(model.size()>0){
            //Muestro y oculto views
            grafico.setVisibility(View.VISIBLE);
            texto_main_sin_datos.setText("PERFIL DE JUGADOR");
            texto_main_sin_datos.setAllCaps(true);
            texto_main_sin_datos.setVisibility(View.VISIBLE);
            texto_main_sin_datos.setTypeface(tf);
            text_loser.setTypeface(tf);
            text_winner.setTypeface(tf);
            text_loser.setVisibility(View.VISIBLE);
            text_winner.setVisibility(View.VISIBLE);
        }
        ataqueModel.setAtacks(atack);
        defensaModel.setAtacks(def);
        transicionModel.setAtacks(tran);
        saqueModel.setAtacks(saq);
        posicionModel.setAtacks(pos);
        resultadoModel.setAtacks(resu);
        suma_numero_registros = resu.size();
        for(int i = 0; i<resu.size();i++){
            if((int)resu.get(i)==1){
                suma_partidos_ganados++;
            }else{
                suma_partidos_perdidos++;
            }
        }

        suma_ataque = suma_ataque/suma_numero_registros;
        suma_defensa = suma_defensa/suma_numero_registros;
        suma_transicion = suma_transicion/suma_numero_registros;
        suma_saque = suma_saque/suma_numero_registros;
        suma_posicion = suma_posicion/suma_numero_registros;

        int perdidos = Integer.parseInt(String.format("%.0f", ((float) suma_partidos_perdidos / suma_numero_registros) * 100));
        int ganados = Integer.parseInt(String.format("%.0f", ((float) suma_partidos_ganados / suma_numero_registros) * 100));

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.circularProgressbar);
        progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.circular));

        ObjectAnimator animation = ObjectAnimator.ofInt (progressBar, "progress", 0, ganados);
        animation.setDuration (1000);
        animation.setInterpolator (new DecelerateInterpolator());
        animation.start ();

        ProgressBar progressBar2 = (ProgressBar) findViewById(R.id.circularProgressbar2);
        progressBar2.setProgressDrawable(getResources().getDrawable(R.drawable.circular2));
        ObjectAnimator animation2 = ObjectAnimator.ofInt (progressBar2, "progress", 0, perdidos);
        animation2.setDuration (1000);
        animation2.setInterpolator (new DecelerateInterpolator());
        animation2.start ();

        text_winner.setText(String.format("%.0f", ((float)suma_partidos_ganados/suma_numero_registros)*100));
        text_winner.setTypeface(tf);

        text_loser.setText(String.format("%.0f", ((float)suma_partidos_perdidos/suma_numero_registros)*100));
        text_loser.setTypeface(tf);

        //*************************************************************************
        //DATOS DEL RADARCHART
        //*************************************************************************
        ArrayList<RadarEntry> arrayDatosEjeY = new ArrayList<RadarEntry>();
        float[] golpes_datos = {suma_ataque,suma_defensa,suma_saque,suma_posicion,suma_transicion};

        for (int i = 0; i < golpes_datos.length; i++)
            arrayDatosEjeY.add(new RadarEntry(golpes_datos[i], i));

        RadarDataSet datosY = new RadarDataSet(arrayDatosEjeY, "Golpes");
        //Contorno del radar
        datosY.setColors(getResources().getColor(R.color.verde_claro));
        //Interior del radar
        datosY.setFillColor(getResources().getColor(R.color.verde_claro));
        datosY.setDrawFilled(true);
        //Transparencia dentro del radar, 0 es totalmente transparente
        datosY.setFillAlpha(150);
        //Grosor del contorno del radar
        datosY.setLineWidth(2f);
        datosY.setDrawHighlightCircleEnabled(true);
        datosY.setDrawHighlightIndicators(false);

        RadarData datos = new RadarData(datosY);
        datos.setValueFormatter(new PercentFormatter());
        //Tamaño texto dentro de radar
        datos.setValueTextSize(15f);
        //Color texto dentro de radar
        datos.setValueTextColor(getResources().getColor(R.color.verde_claro));
        datos.setValueTypeface(tf);
        datos.setDrawValues(false);

        //Valores dentro del radar
        YAxis yAxis = grafico.getYAxis();
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(100f);
        yAxis.setLabelCount(6, true);
        yAxis.setDrawLabels(true);
        yAxis.setTypeface(tf);
        yAxis.setTextColor(getResources().getColor(R.color.colorPrimaryLight));

        //Nombre fuera del radar
        XAxis xAxis = grafico.getXAxis();
        xAxis.setTypeface(tf);
        xAxis.setTextSize(12f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setTextColor(getResources().getColor(R.color.colorPrimaryLight));
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return golpes_nombres[(int) value % golpes_nombres.length];
            }
            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });

        grafico.setData(datos);
        //Quitar seleccionados
        grafico.highlightValues(null);
        //Actualiza gráfico
        grafico.invalidate();
    }

    @Override
    public void getDataUserError() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainActivityPresenter.onDestroy();
    }
}
