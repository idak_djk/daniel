package applepiedev.com.padelstats.Main.Implementations;

import android.content.Context;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import applepiedev.com.padelstats.Main.Interfaces.MainActivityPresenter;
import applepiedev.com.padelstats.Main.Interfaces.MainActivityRepository;
import applepiedev.com.padelstats.Main.Interfaces.MainActivityView;
import applepiedev.com.padelstats.Main.events.MainActivityEvents;
import applepiedev.com.padelstats.Models.GlobalsModel;
import applepiedev.com.padelstats.lib.eventbus.Eventbus;
import applepiedev.com.padelstats.lib.eventbus.GreenRobotEventbus;

/**
 * Created by Daniel on 24/02/2017.
 */

public class MainActivityPresenterImpl implements MainActivityPresenter {
    Eventbus eventbus;
    Context context;
    private MainActivityView mainActivityView;
    private MainActivityRepository mainActivityRepository;

    public MainActivityPresenterImpl(MainActivityView mainActivityView, Context context) {
        this.mainActivityView = mainActivityView;
        this.context = context;
        this.mainActivityRepository = new MainActivityRepositoryImpl();
        this.eventbus = GreenRobotEventbus.getInstance();
    }

    @Override
    public void onCreate() {
        eventbus.register(this);
    }

    @Override
    public void onDestroy() {
        mainActivityView = null;
        eventbus.unregister(this);
    }

    @Override
    public void getDataUser(String userId) {
        mainActivityRepository.getDataUser(userId);
    }

    @Override
    public void getDataUserSuccess(List<GlobalsModel> model) {
        mainActivityView.getDataUserSuccess(model);
    }

    @Override
    public void getDataUserError() {
        mainActivityView.getDataUserError();
    }
    @Subscribe
    @Override
    public void onEventMainThread(MainActivityEvents event) {
        switch (event.getEventType()) {
            case MainActivityEvents.ongetDataSuccess:
                getDataUserSuccess(event.getGlobalModel());
                break;
            case MainActivityEvents.ongetDataError:
                getDataUserError();
                break;
        }
    }
}
