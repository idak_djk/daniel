package applepiedev.com.padelstats.Main.Interfaces;

import java.util.List;

import applepiedev.com.padelstats.Main.events.MainActivityEvents;
import applepiedev.com.padelstats.Models.GlobalsModel;

/**
 * Created by Daniel on 24/02/2017.
 */

public interface MainActivityPresenter {
    // Ciclo de vida de presenter
    void onCreate();
    void onDestroy();
    // Obtención de datos
    void getDataUser(String userId);
    void getDataUserSuccess(List<GlobalsModel> model);
    void getDataUserError();

    // =========== Events =============
    void onEventMainThread(MainActivityEvents event);
}
