package applepiedev.com.padelstats.Main.events;

import java.util.List;

import applepiedev.com.padelstats.Models.GlobalsModel;

/**
 * Created by Daniel on 24/02/2017.
 */

public class MainActivityEvents {
    public static final int ongetDataSuccess = 1;
    public static final int ongetDataError = 2;

    private int eventType;
    private String errorMessage;
    private String name;
    private String src;
    private List<GlobalsModel> globalModel;

    public List<GlobalsModel> getGlobalModel() {
        return globalModel;
    }

    public void setGlobalModel(List<GlobalsModel> globalModel) {
        this.globalModel = globalModel;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}

