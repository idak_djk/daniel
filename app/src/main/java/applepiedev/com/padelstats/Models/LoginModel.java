package applepiedev.com.padelstats.Models;

import java.io.Serializable;

/**
 * Created by idak_ on 05/04/2017.
 */

public class LoginModel implements Serializable {
    private String id_user;
    private String name_user;
    private String email_user;
    private String url_pic_user;
    private String level_user;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getEmail_user() {
        return email_user;
    }

    public void setEmail_user(String email_user) {
        this.email_user = email_user;
    }

    public String getUrl_pic_user() {
        return url_pic_user;
    }

    public void setUrl_pic_user(String url_pic_user) {
        this.url_pic_user = url_pic_user;
    }

    public String getLevel_user() {
        return level_user;
    }

    public void setLevel_user(String level_user) {
        this.level_user = level_user;
    }
}
