package applepiedev.com.padelstats.Models;

import java.io.Serializable;

/**
 * Created by Daniel on 09/03/2017.
 */

public class MatchModel implements Serializable {
    public float getAtaque() {
        return ataque;
    }

    public void setAtaque(float ataque) {
        this.ataque = ataque;
    }

    public float getDefensa() {
        return defensa;
    }

    public void setDefensa(float defensa) {
        this.defensa = defensa;
    }

    public float getTransicion() {
        return transicion;
    }

    public void setTransicion(float transicion) {
        this.transicion = transicion;
    }

    public float getSaque() {
        return saque;
    }

    public void setSaque(float saque) {
        this.saque = saque;
    }

    public float getPosicion() {
        return posicion;
    }

    public void setPosicion(float posicion) {
        this.posicion = posicion;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    private float ataque,defensa,transicion,saque,posicion;
    private int resultado;
}
