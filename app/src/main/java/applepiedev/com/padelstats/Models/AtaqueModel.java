package applepiedev.com.padelstats.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Daniel on 24/02/2017.
 */

public class AtaqueModel implements Serializable {
    ArrayList<Float> atacks;

    public ArrayList<Float> getAtacks() {
        return atacks;
    }

    public void setAtacks(ArrayList<Float> atacks) {
        this.atacks = atacks;
    }
}
