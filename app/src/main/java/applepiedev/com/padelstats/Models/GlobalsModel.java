package applepiedev.com.padelstats.Models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Daniel on 24/02/2017.
 */

public class GlobalsModel implements Serializable {
    @SerializedName("ataque")
    float ataques;
    @SerializedName("defensa")
    float defensas;
    @SerializedName("transicion")
    float transiciones;
    @SerializedName("saque")
    float saques;
    @SerializedName("posicion")
    float posicions;

    public float getDefensas() {
        return defensas;
    }

    public void setDefensas(float defensas) {
        this.defensas = defensas;
    }

    public float getTransiciones() {
        return transiciones;
    }

    public void setTransiciones(float transiciones) {
        this.transiciones = transiciones;
    }

    public float getSaques() {
        return saques;
    }

    public void setSaques(float saques) {
        this.saques = saques;
    }

    public float getPosicions() {
        return posicions;
    }

    public void setPosicions(float posicions) {
        this.posicions = posicions;
    }

    public int getResultados() {
        return resultados;
    }

    public void setResultados(int resultados) {
        this.resultados = resultados;
    }

    @SerializedName("resultado")
    int resultados;

    public float getAtaques() {
        return ataques;
    }

    public void setAtaques(float ataques) {
        this.ataques = ataques;
    }
}
