package applepiedev.com.padelstats.Models;

import java.io.Serializable;

/**
 * Created by idak_ on 07/05/2017.
 */

public class RegisterModel implements Serializable {
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    private String fullname;
    private String email;
    private String pass;
}
