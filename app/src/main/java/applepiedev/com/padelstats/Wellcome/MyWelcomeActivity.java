package applepiedev.com.padelstats.Wellcome;

import com.stephentuso.welcome.WelcomeScreenBuilder;
import com.stephentuso.welcome.ui.WelcomeActivity;
import com.stephentuso.welcome.util.WelcomeScreenConfiguration;

import applepiedev.com.padelstats.R;

/**
 * Created by Ruben on 01/08/2016.
 */
public class MyWelcomeActivity extends WelcomeActivity {
    private String str_welcome_1, str_welcome_2_title, str_welcome_2, str_welcome_3_title, str_welcome_3,
            str_welcome_4_title, str_welcome_4;
    @Override
    protected WelcomeScreenConfiguration configuration() {

        str_welcome_1 = getResources().getString(R.string.str_welcome_1);
        str_welcome_2_title = getResources().getString(R.string.str_welcome_2_title);
        str_welcome_2 = getResources().getString(R.string.str_welcome_2);
        str_welcome_3_title = getResources().getString(R.string.str_welcome_3_title);
        str_welcome_3 = getResources().getString(R.string.str_welcome_3);
        str_welcome_4_title = getResources().getString(R.string.str_welcome_4_title);
        str_welcome_4 = getResources().getString(R.string.str_welcome_4);

        return new WelcomeScreenBuilder(this)
                .theme(R.style.WizardWelcomeScreenTheme_Light)
                .defaultTitleTypefacePath("roboto.ttf")
                .defaultDescriptionTypefacePath("roboto.ttf")
                .doneButtonTypefacePath("roboto.ttf")
                .skipButtonTypefacePath("roboto.ttf")
                .defaultHeaderTypefacePath("roboto.ttf")
                .titlePage(R.drawable.welcome_1, str_welcome_1, R.color.colorPrimary)
                .parallaxPage(R.layout.parallax_2, str_welcome_2_title, str_welcome_2, R.color.colorPrimary, 0.2f, 1f)
                .basicPage(R.drawable.welcome_3, str_welcome_3_title, str_welcome_3_title, R.color.colorPrimary)
                .basicPage(R.drawable.welcome_4, str_welcome_4_title, str_welcome_4, R.color.colorPrimary)
                .swipeToDismiss(true)
                .exitAnimation(android.R.anim.fade_out)
                .build();
    }

    public static String welcomeKey() {
        return "WelcomeScreen";
    }
}