package applepiedev.com.padelstats.Circulo;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Daniel on 14/06/2017.
 */

public class CircleSeekBarListener implements CircularSeekBar.OnCircularSeekBarChangeListener {

    private Context ctx;
    private TextView score;

    public CircleSeekBarListener(Context ctx, TextView score) {
        this.ctx = ctx;
        this.score = score;
    }
    @Override
    public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
        // TODO Insert your code here
        // Toast.makeText(getApplicationContext(),"CAMBIA", Toast.LENGTH_SHORT).show();
        if (progress>=0 && progress<=10)
            score.setText("0");

        if (progress>10 && progress<=24)
            score.setText("1");

        if (progress>24 && progress<=38)
            score.setText("2");

        if (progress>38 && progress<=52)
            score.setText("3");

        if (progress>52 && progress<=66)
            score.setText("4");

        if (progress>66 && progress<=80)
            score.setText("5");

        if (progress>80 && progress<=94)
            score.setText("6");

        if (progress>94 && progress<=100)
            score.setText("7");
    }

    @Override
    public void onStopTrackingTouch(CircularSeekBar seekBar) {
        /*Toast.makeText(ctx,score.getText(),
                Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onStartTrackingTouch(CircularSeekBar seekBar) {


    }
}
