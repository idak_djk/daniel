package applepiedev.com.padelstats.BBDD;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ruben on 12/11/2016.
 */

public class SQLDataBase extends SQLiteOpenHelper {

    //Sentencia SQL para crear la tabla de Estadisticas. Primera columna id autoincremental
    String sqlCreate = "CREATE TABLE Estadisticas (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ataque FLOAT, defensa FLOAT, transicion FLOAT, saque FLOAT, posicion FLOAT, resultado INTEGER, day INTEGER, month INTEGER, year INTEGER, time TEXT)";

    public SQLDataBase(Context contexto, String nombre,
                       SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Se ejecuta la sentencia SQL de creación de la tabla
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior,
                          int versionNueva) {
        //NOTA: Por simplicidad del ejemplo aquí utilizamos directamente
        //      la opción de eliminar la tabla anterior y crearla de nuevo
        //      vacía con el nuevo formato.
        //      Sin embargo lo normal será que haya que migrar datos de la
        //      tabla antigua a la nueva, por lo que este método debería
        //      ser más elaborado.

        //Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS Estadisticas");

        //Se crea la nueva versión de la tabla
        db.execSQL(sqlCreate);
    }
}