package applepiedev.com.padelstats.Stepper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fcannizzaro.materialstepper.AbstractStep;
import com.github.ornolfr.ratingview.RatingView;

import applepiedev.com.padelstats.R;

public class Paso_a_paso_Zona_saque_posicion extends AbstractStep {

    public static  RatingView rating_saque, rating_posicion;
    public static Float float_saque= 1.0f;
    public static Float float_posicion= 1.0f;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.paso_a_paso_saque_posicion, container, false);

        rating_saque = (RatingView) v.findViewById(R.id.rating_saque);
        rating_posicion = (RatingView) v.findViewById(R.id.rating_posicion);

        TextView txt_saque = (TextView) v.findViewById(R.id.txt_saque);
        TextView txt_posicion = (TextView) v.findViewById(R.id.txt_posicion);
        TextView txt_zona_saque = (TextView) v.findViewById(R.id.txt_zona_saque);
        TextView txt_zona_posicion = (TextView) v.findViewById(R.id.txt_zona_posicion);


        Typeface tipografia = Typeface.createFromAsset(getContext().getAssets(),"arista.ttf");

        txt_saque.setTypeface(tipografia);
        txt_posicion.setTypeface(tipografia);
        txt_zona_saque.setTypeface(tipografia);
        txt_zona_posicion.setTypeface(tipografia);

        //Si se ha pulsado en alguno de los ratingview:
        if (float_saque!=1 || float_posicion!=1) {
            SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Saque_Posicion", Context.MODE_PRIVATE);
            float_saque = prefs.getFloat("saque",1);
            float_posicion = prefs.getFloat("posicion",1);
            rating_saque.setRating(float_saque);
            rating_posicion.setRating(float_posicion);
        }


        rating_saque.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_saque = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Saque_Posicion", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("saque", float_saque);
                editor.commit();
            }
        });
        rating_posicion.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_posicion = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Saque_Posicion", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("posicion", float_posicion);
                editor.commit();
            }
        });

        return v;
    }


   @Override
    public String name() {
        return "Zona";
    }

}
