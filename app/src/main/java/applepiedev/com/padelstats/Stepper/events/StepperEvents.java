package applepiedev.com.padelstats.Stepper.events;

/**
 * Created by Daniel on 06/03/2017.
 */

public class StepperEvents {
    public static final int onSetNewDataSuccess = 1;
    public static final int onSetNewDataError = 2;

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    private int eventType;
    private String errorMessage;
}
