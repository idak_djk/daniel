package applepiedev.com.padelstats.Stepper.Interfaces;

import applepiedev.com.padelstats.Models.MatchModel;

/**
 * Created by Daniel on 06/03/2017.
 */

public interface StepperRepository {
    // Inserción de datos
    void setNewData(MatchModel matchModel, String userId);
}
