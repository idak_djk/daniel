package applepiedev.com.padelstats.Stepper;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.github.fcannizzaro.materialstepper.AbstractStep;
import com.github.fcannizzaro.materialstepper.style.DotStepper;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.Calendar;

import applepiedev.com.padelstats.BBDD.SQLDataBase;
import applepiedev.com.padelstats.Circulo.CircularSeekBar;
import applepiedev.com.padelstats.Main.Interfaces.MainActivityPresenter;
import applepiedev.com.padelstats.Main.MainActivity;
import applepiedev.com.padelstats.Models.MatchModel;
import applepiedev.com.padelstats.R;
import applepiedev.com.padelstats.RadarMarkerView;
import applepiedev.com.padelstats.Stepper.Implementations.StepperPresenterImpl;
import applepiedev.com.padelstats.Stepper.Interfaces.StepperPresenter;

import static com.facebook.FacebookSdk.getApplicationContext;

public class DotsSample extends DotStepper {

    private int i = 1;
    private RadarChart grafico_dialogo;
    public Typeface tf;
    public TextView titulo_dialogo, dialog_ataque, dialog_defensa, dialog_transicion,
            dialog_saque, dialog_posicion;
    public String[] golpes_nombres;
    private SQLiteDatabase db;
    private Float ataque, defensa, transicion, posicion, saque;
    private StepperPresenter stepperPresenter;
    private MatchModel matchModel;
    private String userId;

    private CardView crd_set_3;
    private Paso_a_paso_Resultado_partido resultFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setErrorTimeout(1500);
        setTitle("Configurador de estadísticas");
        // Obtenemos los extras (USER_ID)
        Bundle extras = getIntent().getExtras();
        userId = (String) extras.get("USER_ID");

        tf = Typeface.createFromAsset(getAssets(), "arista.ttf");

        resultFragment = new Paso_a_paso_Resultado_partido();

        //addStep(createFragment(resultFragment));
        addStep(createFragment(new Paso_a_paso_Zona_ataque()));
        addStep(createFragment(new Paso_a_paso_Zona_defensa()));
        addStep(createFragment(new Paso_a_paso_Zona_transicion()));
        addStep(createFragment(new Paso_a_paso_Zona_saque_posicion()));

        stepperPresenter = new StepperPresenterImpl(this, DotsSample.this);
        stepperPresenter.onCreate();

        super.onCreate(savedInstanceState);

        // Generamos un nuevo modelo de partido
        matchModel = new MatchModel();

        //Abrimos la base de datos 'DBEstadisticas' en modo escritura
        SQLDataBase sqldb = new SQLDataBase(this, "DBEstadisticas", null, 1);
        db = sqldb.getWritableDatabase();

        //Tipografia para los botones del stepper
        TextView siguiente = (TextView) findViewById(com.github.fcannizzaro.materialstepper.R.id.stepNext);
        TextView anterior = (TextView) findViewById(com.github.fcannizzaro.materialstepper.R.id.stepPrev);
        TextView guardar = (TextView) findViewById(com.github.fcannizzaro.materialstepper.R.id.stepEnd);
        ViewSwitcher switcher_layout = (ViewSwitcher) findViewById(com.github.fcannizzaro.materialstepper.R.id.stepSwitcher);
        siguiente.setTypeface(tf);
        siguiente.setTextColor(getResources().getColor(R.color.colorBlanco));
        anterior.setTypeface(tf);
        anterior.setTextColor(getResources().getColor(R.color.colorBlanco));
        guardar.setTypeface(tf);
        guardar.setTextColor(getResources().getColor(R.color.colorBlanco));
        switcher_layout.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

    }

    private AbstractStep createFragment(AbstractStep fragment) {
        Bundle b = new Bundle();
        b.putInt("position", i++);
        fragment.setArguments(b);
        return fragment;
    }
   /* @Override
    public void onBackPressed() {
    }*/

    @Override
    public void onComplete() {
        super.onComplete();

        //Muestro dialogo
        final Dialog dialogo_stepper = new Dialog(this, R.style.DialogTheme);
        dialogo_stepper.setContentView(R.layout.dialogo_stepper);
        dialogo_stepper.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        grafico_dialogo = (RadarChart) dialogo_stepper.findViewById(R.id.grafico_dialogo);
        titulo_dialogo = (TextView) dialogo_stepper.findViewById(R.id.main_title);
        dialog_ataque = (TextView) dialogo_stepper.findViewById(R.id.dialog_ataque);
        dialog_defensa = (TextView) dialogo_stepper.findViewById(R.id.dialog_defensa);
        dialog_transicion = (TextView) dialogo_stepper.findViewById(R.id.dialog_transicion);
        dialog_saque = (TextView) dialogo_stepper.findViewById(R.id.dialog_saque);
        dialog_posicion = (TextView) dialogo_stepper.findViewById(R.id.dialog_posicion);

        crd_set_3 = (CardView) findViewById(R.id.crd_set_3);

        //Mostramos la view radarchart
        mostrarGrafico();

        dialog_ataque.setText(String.format("%.1f", ataque) + "%");
        dialog_defensa.setText(String.format("%.1f", defensa) + "%");
        dialog_transicion.setText(String.format("%.1f", transicion) + "%");
        dialog_saque.setText(String.format("%.1f", saque) + "%");
        dialog_posicion.setText(String.format("%.1f", posicion) + "%");

        titulo_dialogo.setTypeface(tf);
        dialog_ataque.setTypeface(tf);
        dialog_defensa.setTypeface(tf);
        dialog_transicion.setTypeface(tf);
        dialog_saque.setTypeface(tf);
        dialog_posicion.setTypeface(tf);

        //Boton aceptar
        Button btn_dialogo_stepper_ok = (Button) dialogo_stepper.findViewById(R.id.btn_dialogo_stepper_ok);
        btn_dialogo_stepper_ok.setTypeface(tf);
        btn_dialogo_stepper_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //Si no hemos pulsado en partido ganado o perdido:
                if (Paso_a_paso_Resultado_partido.resultado == 0) {
                    //Salimos del dialogo
                    dialogo_stepper.dismiss();
                    //Montamos el custom toast
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));
                    TextView text = (TextView) layout.findViewById(R.id.txt_toast);
                    text.setText(getResources().getString(R.string.pregunta_partidos));
                    text.setTypeface(tf);
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 10);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                } else {
                    //Elimino el dialogo para prevenir leaked window
                    dialogo_stepper.dismiss();
                    //Obtengo los datos
                    final Calendar cal = Calendar.getInstance();
                    int year = Integer.parseInt(String.valueOf(cal.get(Calendar.YEAR)));
                    int month = Integer.parseInt(String.valueOf((cal.get(Calendar.MONTH) + 1)));
                    int day = Integer.parseInt(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
                    String hour = String.format("%02d", cal.get(Calendar.HOUR_OF_DAY));
                    String minute = String.format("%02d", cal.get(Calendar.MINUTE));
                    String time = hour + ":" + minute;

                    //Metodo insert()
                    ContentValues nuevoRegistro = new ContentValues();
                    matchModel.setAtaque(ataque);
                    matchModel.setDefensa(defensa);
                    matchModel.setTransicion(transicion);
                    matchModel.setSaque(saque);
                    matchModel.setPosicion(posicion);
                    matchModel.setResultado(Paso_a_paso_Resultado_partido.resultado);
                    // Base de datos local
                    nuevoRegistro.put("ataque", ataque);
                    nuevoRegistro.put("defensa", defensa);
                    nuevoRegistro.put("transicion", transicion);
                    nuevoRegistro.put("saque", saque);
                    nuevoRegistro.put("posicion", posicion);
                    nuevoRegistro.put("resultado", Paso_a_paso_Resultado_partido.resultado);
                    nuevoRegistro.put("day", day);
                    nuevoRegistro.put("month", month);
                    nuevoRegistro.put("year", year);
                    nuevoRegistro.put("time", time);
                    db.insert("Estadisticas", null, nuevoRegistro);

                    stepperPresenter.onCreate();
                    stepperPresenter.setNewData(matchModel,userId);


                    //Montamos el custom toast
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));
                    TextView text = (TextView) layout.findViewById(R.id.txt_toast);
                    text.setText("Registro insertado con éxito: " + " - A: " + ataque + " - D: " + defensa + " - T: " + transicion + " - S: " + saque + " - P: " + posicion + " - Fecha: " + day + "/" + month + "/ " + year + " - Hora: " + time + "h - Resultado: " + Paso_a_paso_Resultado_partido.resultado);
                    text.setTypeface(tf);
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 10);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    //Vamos a Main
                    Intent intent = new Intent(DotsSample.this, MainActivity.class);
                    startActivity(intent);

                    //Con esto matamos el hilo del MainActivity y crea de nuevo la Activity sin que quede nada en segundo plano (ninguna activity abierta)
                    //Asi hemos ido con el hilo de Main hasta aquí sin cerrar nada, puediendo volver a Main
                    setResult(RESULT_OK);

                    //INICIALIZAMOS VALORES DE LOS RATINGSVIEW PARA LA PROXIMA VEZ QUE ENTREMOS
                    Paso_a_paso_Resultado_partido.resultado = 0;
                    Paso_a_paso_Zona_ataque.rating_samsh.setRating(1.0f);
                    Paso_a_paso_Zona_ataque.rating_dejada.setRating(1.0f);
                    Paso_a_paso_Zona_ataque.rating_volea.setRating(1.0f);
                    Paso_a_paso_Zona_ataque.float_smash = 1.0f;
                    Paso_a_paso_Zona_ataque.float_dejada = 1.0f;
                    Paso_a_paso_Zona_ataque.float_volea = 1.0f;
                    Paso_a_paso_Zona_defensa.rating_globo.setRating(1.0f);
                    Paso_a_paso_Zona_defensa.rating_resto.setRating(1.0f);
                    Paso_a_paso_Zona_defensa.rating_salida_pared.setRating(1.0f);
                    Paso_a_paso_Zona_defensa.float_globo = 1.0f;
                    Paso_a_paso_Zona_defensa.float_resto = 1.0f;
                    Paso_a_paso_Zona_defensa.float_salida_pared = 1.0f;
                    Paso_a_paso_Zona_transicion.rating_drive.setRating(1.0f);
                    Paso_a_paso_Zona_transicion.rating_reves.setRating(1.0f);
                    Paso_a_paso_Zona_transicion.rating_bandeja.setRating(1.0f);
                    Paso_a_paso_Zona_transicion.rating_bote_pronto.setRating(1.0f);
                    Paso_a_paso_Zona_transicion.float_drive = 1.0f;
                    Paso_a_paso_Zona_transicion.float_reves = 1.0f;
                    Paso_a_paso_Zona_transicion.float_bandeja = 1.0f;
                    Paso_a_paso_Zona_transicion.float_bote_pronto = 1.0f;
                    Paso_a_paso_Zona_saque_posicion.rating_saque.setRating(1.0f);
                    Paso_a_paso_Zona_saque_posicion.rating_posicion.setRating(1.0f);
                    Paso_a_paso_Zona_saque_posicion.float_saque = 1.0f;
                    Paso_a_paso_Zona_saque_posicion.float_posicion = 1.0f;

                    //Matamos el hilo
                    finish();
                }
            }
        });

        //Boton cancelar
        Button btn_dialogo_stepper_cancel = (Button) dialogo_stepper.findViewById(R.id.btn_dialogo_stepper_cancel);
        btn_dialogo_stepper_cancel.setTypeface(tf);
        btn_dialogo_stepper_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialogo_stepper.dismiss();
            }
        });
        dialogo_stepper.show();
    }

    private void mostrarGrafico() {
        //==========================================================================
        // Añadimos el gráfico de Rubén RadarChart
        //==========================================================================
        golpes_nombres = new String[]{getString(R.string.ataque), getString(R.string.defensa),
                getString(R.string.saque), getString(R.string.posicion), getString(R.string.transicion)};
        grafico_dialogo.setDescription(null);
        grafico_dialogo.setWebLineWidth(2f);
        //Lineas desde el centro
        grafico_dialogo.setWebColor(getResources().getColor(R.color.colorBlanco));
        grafico_dialogo.setWebLineWidthInner(2f);
        //Lineas desde vertices
        grafico_dialogo.setWebColorInner(getResources().getColor(R.color.colorBlanco));
        grafico_dialogo.setWebAlpha(100);
        // Marker customizado
        MarkerView mv = new RadarMarkerView(this, R.layout.radar_marker);
        mv.setChartView(grafico_dialogo);
        grafico_dialogo.setMarker(mv);
        grafico_dialogo.animateY(1400);

        //*************************************************************************
        //DATOS DEL RADARCHART
        //*************************************************************************
        ArrayList<RadarEntry> arrayDatosEjeY = new ArrayList<RadarEntry>();
        //Cogemos datos del stepper y aplicamos una conversión a %
        ataque = (Paso_a_paso_Zona_ataque.float_smash + Paso_a_paso_Zona_ataque.float_dejada + Paso_a_paso_Zona_ataque.float_volea) * 100 / 15;
        defensa = (Paso_a_paso_Zona_defensa.float_globo + Paso_a_paso_Zona_defensa.float_resto + Paso_a_paso_Zona_defensa.float_salida_pared) * 100 / 15;
        saque = (Paso_a_paso_Zona_saque_posicion.float_saque) * 20;
        posicion = (Paso_a_paso_Zona_saque_posicion.float_posicion) * 20;
        transicion = (Paso_a_paso_Zona_transicion.float_drive + Paso_a_paso_Zona_transicion.float_reves + Paso_a_paso_Zona_transicion.float_bandeja + Paso_a_paso_Zona_transicion.float_bote_pronto) * 5;


        float[] golpes_datos = {ataque, defensa, saque, posicion, transicion};
        for (int i = 0; i < golpes_datos.length; i++)
            arrayDatosEjeY.add(new RadarEntry(golpes_datos[i], i));

        RadarDataSet datosY = new RadarDataSet(arrayDatosEjeY, "Golpes");

        switch (Paso_a_paso_Resultado_partido.resultado) {
            case 1:
                //Contorno del radar
                datosY.setColors(getResources().getColor(R.color.azul_claro));
                //Interior del radar
                datosY.setFillColor(getResources().getColor(R.color.azul_claro));
                break;
            case 2:
                //Contorno del radar
                datosY.setColors(getResources().getColor(R.color.rojo_claro));
                //Interior del radar
                datosY.setFillColor(getResources().getColor(R.color.rojo_claro));
                break;
            default:
                //Contorno del radar
                datosY.setColors(getResources().getColor(R.color.azul_claro));
                //Interior del radar
                datosY.setFillColor(getResources().getColor(R.color.azul_claro));
                break;
        }
        datosY.setDrawFilled(true);
        //Transparencia dentro del radar, 0 es totalmente transparente
        datosY.setFillAlpha(150);
        //Grosor del contorno del radar
        datosY.setLineWidth(2f);
        datosY.setDrawHighlightCircleEnabled(true);
        datosY.setDrawHighlightIndicators(false);

        RadarData datos = new RadarData(datosY);
        datos.setValueFormatter(new PercentFormatter());
        //Tamaño texto dentro de radar
        datos.setValueTextSize(15f);
        //Color texto dentro de radar
        datos.setValueTextColor(getResources().getColor(R.color.colorPrimary));
        datos.setValueTypeface(tf);
        datos.setDrawValues(false);

        //Valores dentro del radar
        YAxis yAxis = grafico_dialogo.getYAxis();
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(100f);
        yAxis.setLabelCount(6, true);
        yAxis.setDrawLabels(true);
        yAxis.setTypeface(tf);
        yAxis.setTextColor(getResources().getColor(R.color.colorPrimary));

        //Nombre fuera del radar
        XAxis xAxis = grafico_dialogo.getXAxis();
        xAxis.setTypeface(tf);
        xAxis.setTextSize(12f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setTextColor(getResources().getColor(R.color.colorPrimary));
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return golpes_nombres[(int) value % golpes_nombres.length];
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });

        grafico_dialogo.setData(datos);
        //Quitar seleccionados
        grafico_dialogo.highlightValues(null);
        //Actualiza gráfico
        grafico_dialogo.invalidate();

        Legend leyenda = grafico_dialogo.getLegend();
        leyenda.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        leyenda.setXEntrySpace(7);
        leyenda.setYEntrySpace(5);
        //Ocultar leyenda
        leyenda.setEnabled(false);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stepperPresenter.onDestroy();
    }

    public void setNewDataSuccess(){
        Toast.makeText(this,"INTRODUCIDO CORRECTAMENTE",Toast.LENGTH_LONG).show();
    }

    public void setNewDataError(){
        Toast.makeText(this,"ERROR AL INSERTAR DATOS",Toast.LENGTH_LONG).show();
    }

    public void two_sets(View v){
        resultFragment.setThreeSetVisibility(false);
    }

    public void three_sets(View v){
        resultFragment.setThreeSetVisibility(true);
    }
}
