package applepiedev.com.padelstats.Stepper;

import android.app.Fragment;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import applepiedev.com.padelstats.R;
import applepiedev.com.padelstats.Stepper.Interfaces.StepperView;

public class StepperActivity extends AppCompatActivity implements StepperView {

    private int position = 0;
    private Paso_a_paso_Resultado_partido paso_a_paso_Resultado_partido;
    private Fragment paso_a_paso_Zona_ataque;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stepper);
        changeFragment();
        updateTitle();
    }

    @Override
    public void goToNextFragment() {
        switch (position) {
            case 0:
                position++;
                changeFragment();
                updateTitle();
                break;
            case 1:
                position++;
                changeFragment();
                updateTitle();
                break;
        }
    }

    @Override
    public void changeFragment() {
        Bundle bundle = new Bundle();
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (position) {
            case 0:
                if (paso_a_paso_Resultado_partido == null) {
                    paso_a_paso_Resultado_partido = new Paso_a_paso_Resultado_partido();
                }
                    fragmentTransaction.replace(R.id.fragment_container, paso_a_paso_Resultado_partido, "Resultado");
                break;
            /*case 1:
                if (paso_a_paso_Zona_ataque == null) {
                    paso_a_paso_Zona_ataque = new Paso_a_paso_Zona_ataque();
                }
                fragmentTransaction.replace(R.id.fragment_container, paso_a_paso_Zona_ataque, "Ataque");
                break;*/
        }
        fragmentTransaction.addToBackStack(null).commit();
    }

    @Override
    public void updateTitle() {

    }

    public Context getContext() {
        return this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_stepper, menu);
        return true;
    }
}
