package applepiedev.com.padelstats.Stepper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fcannizzaro.materialstepper.AbstractStep;
import com.github.ornolfr.ratingview.RatingView;

import applepiedev.com.padelstats.R;

public class Paso_a_paso_Zona_defensa extends AbstractStep {

    public static  RatingView rating_globo, rating_resto, rating_salida_pared;
    public static Float float_globo = 1.0f;
    public static Float float_resto = 1.0f;
    public static Float float_salida_pared = 1.0f;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.paso_a_paso_defensa, container, false);
        rating_globo = (RatingView) v.findViewById(R.id.rating_globo);
        rating_resto = (RatingView) v.findViewById(R.id.rating_resto);
        rating_salida_pared = (RatingView) v.findViewById(R.id.rating_salida_pared);

        TextView txt_globo = (TextView) v.findViewById(R.id.txt_globo);
        TextView txt_resto = (TextView) v.findViewById(R.id.txt_resto);
        TextView txt_salida_pared = (TextView) v.findViewById(R.id.txt_salida_pared);
        TextView txt_zona = (TextView) v.findViewById(R.id.txt_zona);

        Typeface tipografia = Typeface.createFromAsset(getContext().getAssets(),"arista.ttf");

        txt_globo.setTypeface(tipografia);
        txt_resto.setTypeface(tipografia);
        txt_salida_pared.setTypeface(tipografia);
        txt_zona.setTypeface(tipografia);

        //Si se ha pulsado en alguno de los ratingview:
        if (float_globo!=1 || float_resto!=1 || float_salida_pared!=1) {
            SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Defensa", Context.MODE_PRIVATE);
            float_globo = prefs.getFloat("globo",1);
            float_resto = prefs.getFloat("resto",1);
            float_salida_pared = prefs.getFloat("salida",1);
            rating_globo.setRating(float_globo);
            rating_resto.setRating(float_resto);
            rating_salida_pared.setRating(float_salida_pared);
        }

        rating_globo.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_globo = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Defensa", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("globo", float_globo);
                editor.commit();
            }
        });
        rating_resto.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_resto = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Defensa", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("resto", float_resto);
                editor.commit();
            }
        });
        rating_salida_pared.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_salida_pared = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Defensa", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("salida", float_salida_pared);
                editor.commit();
            }
        });
        return v;
    }


   @Override
    public String name() {
        return "Zona";
    }

}
