package applepiedev.com.padelstats.Stepper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fcannizzaro.materialstepper.AbstractStep;
import com.github.ornolfr.ratingview.RatingView;

import applepiedev.com.padelstats.R;

public class Paso_a_paso_Zona_transicion extends AbstractStep {

    public static  RatingView rating_drive, rating_reves, rating_bandeja, rating_bote_pronto;
    public static Float float_drive = 1.0f;
    public static Float float_reves= 1.0f;
    public static Float float_bandeja= 1.0f;
    public static Float float_bote_pronto= 1.0f;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.paso_a_paso_transicion, container, false);

        rating_drive = (RatingView) v.findViewById(R.id.rating_drive);
        rating_reves = (RatingView) v.findViewById(R.id.rating_reves);
        rating_bandeja = (RatingView) v.findViewById(R.id.rating_bandeja);
        rating_bote_pronto = (RatingView) v.findViewById(R.id.rating_bote_pronto);

        TextView txt_drive = (TextView) v.findViewById(R.id.txt_drive);
        TextView txt_reves = (TextView) v.findViewById(R.id.txt_reves);
        TextView txt_bandeja = (TextView) v.findViewById(R.id.txt_bandeja);
        TextView txt_bote_pronto = (TextView) v.findViewById(R.id.txt_bote_pronto);
        TextView txt_zona = (TextView) v.findViewById(R.id.txt_zona);

        Typeface tipografia = Typeface.createFromAsset(getContext().getAssets(),"arista.ttf");

        txt_drive.setTypeface(tipografia);
        txt_reves.setTypeface(tipografia);
        txt_bandeja.setTypeface(tipografia);
        txt_bote_pronto.setTypeface(tipografia);
        txt_zona.setTypeface(tipografia);

        //Si se ha pulsado en alguno de los ratingview:
        if (float_drive!=1 || float_reves!=1 || float_bandeja!=1 || float_bote_pronto!=1) {
            SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Transicion", Context.MODE_PRIVATE);
            float_drive = prefs.getFloat("drive",1);
            float_reves = prefs.getFloat("reves",1);
            float_bandeja = prefs.getFloat("bandeja",1);
            float_bote_pronto = prefs.getFloat("bote",1);
            rating_drive.setRating(float_drive);
            rating_reves.setRating(float_reves);
            rating_bandeja.setRating(float_bandeja);
            rating_bote_pronto.setRating(float_bote_pronto);
        }

        rating_drive.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_drive = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Transicion", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("drive", float_drive);
                editor.commit();
            }
        });
        rating_reves.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_reves = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Transicion", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("reves", float_reves);
                editor.commit();
            }
        });
        rating_bandeja.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_bandeja = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Transicion", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("bandeja", float_bandeja);
                editor.commit();
            }
        });
        rating_bote_pronto.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_bote_pronto = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Transicion", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("bote", float_bote_pronto);
                editor.commit();
            }
        });

        return v;
    }


   @Override
    public String name() {
        return "Zona";
    }

}
