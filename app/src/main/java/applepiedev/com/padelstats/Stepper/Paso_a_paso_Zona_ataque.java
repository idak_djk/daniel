package applepiedev.com.padelstats.Stepper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fcannizzaro.materialstepper.AbstractStep;
import com.github.ornolfr.ratingview.RatingView;

import applepiedev.com.padelstats.R;

public class Paso_a_paso_Zona_ataque extends AbstractStep {

    public static RatingView rating_samsh, rating_dejada, rating_volea;
    public static Float float_smash = 1.0f;
    public static Float float_dejada = 1.0f;
    public static Float float_volea = 1.0f;
    private Toolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.paso_a_paso_ataque, container, false);

        //Ocultar actionbar
        toolbar = mStepper.getToolbar();
        if(toolbar==null){
            Log.d("toolbar","null");
        }
        else{
            ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
            ActionBar actionBar = getActionBar();
            actionBar.hide();
        }

        rating_samsh = (RatingView) v.findViewById(R.id.rating_samsh);
        rating_dejada = (RatingView) v.findViewById(R.id.rating_dejada);
        rating_volea = (RatingView) v.findViewById(R.id.rating_volea);

        TextView txt_smash = (TextView) v.findViewById(R.id.txt_smash);
        TextView txt_dejada = (TextView) v.findViewById(R.id.txt_dejada);
        TextView txt_volea = (TextView) v.findViewById(R.id.txt_volea);
        TextView txt_zona = (TextView) v.findViewById(R.id.txt_zona);

        Typeface tipografia = Typeface.createFromAsset(getContext().getAssets(),"arista.ttf");

        txt_smash.setTypeface(tipografia);
        txt_dejada.setTypeface(tipografia);
        txt_volea.setTypeface(tipografia);
        txt_zona.setTypeface(tipografia);

        //Si se ha pulsado en alguno de los ratingview:
        if (float_smash!=1 || float_dejada!=1 || float_volea!=1) {
            SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Ataque", Context.MODE_PRIVATE);
            float_smash = prefs.getFloat("smash",1);
            float_dejada = prefs.getFloat("dejada",1);
            float_volea = prefs.getFloat("volea",1);
            rating_samsh.setRating(float_smash);
            rating_dejada.setRating(float_dejada);
            rating_volea.setRating(float_volea);
        }

        rating_samsh.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_smash = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Ataque", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("smash", float_smash);
                editor.commit();
            }
        });
        rating_dejada.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_dejada = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Ataque", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("dejada", float_dejada);
                editor.commit();
            }
        });
        rating_volea.setOnRatingChangedListener(new RatingView.OnRatingChangedListener() {
            @Override
            public void onRatingChange(float oldRating, float newRating) {
                float_volea = newRating;
                SharedPreferences prefs = getActivity().getSharedPreferences("Zona_Ataque", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putFloat("volea", float_volea);
                editor.commit();
            }
        });


        return v;
    }


    @Override
    public String name() {
        return "Zona";
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }
}
