package applepiedev.com.padelstats.Stepper.Implementations;

import android.content.Context;

import org.greenrobot.eventbus.Subscribe;

import applepiedev.com.padelstats.Main.events.MainActivityEvents;
import applepiedev.com.padelstats.Models.MatchModel;
import applepiedev.com.padelstats.Stepper.DotsSample;
import applepiedev.com.padelstats.Stepper.Interfaces.StepperPresenter;
import applepiedev.com.padelstats.Stepper.Interfaces.StepperRepository;
import applepiedev.com.padelstats.Stepper.events.StepperEvents;
import applepiedev.com.padelstats.lib.eventbus.Eventbus;
import applepiedev.com.padelstats.lib.eventbus.GreenRobotEventbus;

/**
 * Created by Daniel on 06/03/2017.
 */

public class StepperPresenterImpl implements StepperPresenter {
    Eventbus eventbus;
    Context context;
    private DotsSample dotsSampleView;
    private StepperRepository stepperRepository;

    public StepperPresenterImpl(DotsSample dotsSampleView, Context context) {
        this.dotsSampleView = dotsSampleView;
        this.context = context;
        this.stepperRepository = new StepperRepositoryImpl();
        this.eventbus = GreenRobotEventbus.getInstance();
    }

    @Override
    public void setNewData(MatchModel matchModel, String userId) {
        stepperRepository.setNewData(matchModel, userId);
    }

    @Override
    public void setNewDataSuccess() {
        dotsSampleView.setNewDataSuccess();
    }

    @Override
    public void SetNewDataError() {
        dotsSampleView.setNewDataError();
    }

    @Override
    public void onCreate() {
        try {
            eventbus.register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        eventbus.unregister(this);
    }

    @Subscribe
    @Override
    public void onEventMainThread(StepperEvents event) {
        switch (event.getEventType()) {
            case StepperEvents.onSetNewDataSuccess:
                setNewDataSuccess();
                break;
            case StepperEvents.onSetNewDataError:
                SetNewDataError();
                break;
        }

    }
}
