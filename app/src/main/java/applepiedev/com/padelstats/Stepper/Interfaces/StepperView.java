package applepiedev.com.padelstats.Stepper.Interfaces;

/**
 * Created by Daniel on 21/06/2017.
 */

public interface StepperView {
    void goToNextFragment();
    void changeFragment();
    void updateTitle();
}
