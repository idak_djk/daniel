package applepiedev.com.padelstats.Stepper.Interfaces;

import applepiedev.com.padelstats.Models.MatchModel;
import applepiedev.com.padelstats.Stepper.events.StepperEvents;

/**
 * Created by Daniel on 06/03/2017.
 */

public interface StepperPresenter {
    // Inserción de datos
    void setNewData(MatchModel matchModel, String userId);
    void setNewDataSuccess();
    void SetNewDataError();

    void onCreate();
    void onDestroy();

    // =========== Events =============
    void onEventMainThread(StepperEvents event);
}
