package applepiedev.com.padelstats.Stepper;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.fcannizzaro.materialstepper.AbstractStep;

import applepiedev.com.padelstats.Circulo.CircleSeekBarListener;
import applepiedev.com.padelstats.Circulo.CircularSeekBar;
import applepiedev.com.padelstats.R;

public class Paso_a_paso_Resultado_partido extends Fragment {

    public static int resultado = 0;
    private final Context context = getActivity();
    private CardView crd_set_3;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.paso_a_paso_resultado_partido, container, false);
        crd_set_3 = (CardView) v.findViewById(R.id.crd_set_3);

        TextView txt_result_first = (TextView) v.findViewById(R.id.txt_result_first);
        TextView txt_result_second = (TextView) v.findViewById(R.id.txt_result_second);
        Typeface tipografia = Typeface.createFromAsset(getContext().getAssets(), "arista.ttf");
        txt_result_first.setTypeface(tipografia);
        txt_result_second.setTypeface(tipografia);

        /** Score Textviews **/
        TextView score = (TextView) v.findViewById(R.id.score);
        TextView score2 = (TextView) v.findViewById(R.id.score2);
        TextView score3 = (TextView) v.findViewById(R.id.score3);
        TextView score4 = (TextView) v.findViewById(R.id.score4);
        TextView score5 = (TextView) v.findViewById(R.id.score5);
        TextView score6 = (TextView) v.findViewById(R.id.score6);

        /** Declarating CircleSeekbars **/
        CircularSeekBar seekbar = (CircularSeekBar) v.findViewById(R.id.circularSeekBar);
        CircularSeekBar seekbar2 = (CircularSeekBar) v.findViewById(R.id.circularSeekBar2);
        CircularSeekBar seekbar3 = (CircularSeekBar) v.findViewById(R.id.circularSeekBar3);
        CircularSeekBar seekbar4 = (CircularSeekBar) v.findViewById(R.id.circularSeekBar4);
        CircularSeekBar seekbar5 = (CircularSeekBar) v.findViewById(R.id.circularSeekBar5);
        CircularSeekBar seekbar6 = (CircularSeekBar) v.findViewById(R.id.circularSeekBar6);

        /** Setting max and min **/
        // First Set
        seekbar.setMax(100);
        seekbar.setProgress(0);
        seekbar2.setMax(100);
        seekbar2.setProgress(0);
        // Second Set
        seekbar3.setMax(100);
        seekbar3.setProgress(0);
        seekbar4.setMax(100);
        seekbar4.setProgress(0);
        // Third Set
        seekbar5.setMax(100);
        seekbar5.setProgress(0);
        seekbar6.setMax(100);
        seekbar6.setProgress(0);

        /** Setting odd elements colors */
        seekbar.setCircleProgressColor(getResources().getColor(R.color.verde_claro));
        seekbar.setPointerColor(getResources().getColor(R.color.naranja_claro));
        seekbar.setCircleColor(getResources().getColor(R.color.colorPrimaryDark));
        seekbar.setPointerHaloColor(getResources().getColor(R.color.verde_claro));

        seekbar3.setCircleProgressColor(getResources().getColor(R.color.verde_claro));
        seekbar3.setPointerColor(getResources().getColor(R.color.naranja_claro));
        seekbar3.setCircleColor(getResources().getColor(R.color.colorPrimaryDark));
        seekbar3.setPointerHaloColor(getResources().getColor(R.color.verde_claro));

        seekbar5.setCircleProgressColor(getResources().getColor(R.color.verde_claro));
        seekbar5.setPointerColor(getResources().getColor(R.color.naranja_claro));
        seekbar5.setCircleColor(getResources().getColor(R.color.colorPrimaryDark));
        seekbar5.setPointerHaloColor(getResources().getColor(R.color.verde_claro));


        /** Setting even elements colors **/

        seekbar2.setCircleProgressColor(getResources().getColor(R.color.naranja_claro));
        seekbar2.setPointerColor(getResources().getColor(R.color.verde_claro));
        seekbar2.setCircleColor(getResources().getColor(R.color.colorPrimaryDark));
        seekbar2.setPointerHaloColor(getResources().getColor(R.color.naranja_claro));

        seekbar4.setCircleProgressColor(getResources().getColor(R.color.naranja_claro));
        seekbar4.setPointerColor(getResources().getColor(R.color.verde_claro));
        seekbar4.setCircleColor(getResources().getColor(R.color.colorPrimaryDark));
        seekbar4.setPointerHaloColor(getResources().getColor(R.color.naranja_claro));

        seekbar6.setCircleProgressColor(getResources().getColor(R.color.naranja_claro));
        seekbar6.setPointerColor(getResources().getColor(R.color.verde_claro));
        seekbar6.setCircleColor(getResources().getColor(R.color.colorPrimaryDark));
        seekbar6.setPointerHaloColor(getResources().getColor(R.color.naranja_claro));

        /** Setting Adapters **/

        seekbar.setOnSeekBarChangeListener(new CircleSeekBarListener(context, score));
        seekbar2.setOnSeekBarChangeListener(new CircleSeekBarListener(context, score2));
        seekbar3.setOnSeekBarChangeListener(new CircleSeekBarListener(context, score3));
        seekbar4.setOnSeekBarChangeListener(new CircleSeekBarListener(context, score4));
        seekbar5.setOnSeekBarChangeListener(new CircleSeekBarListener(context, score5));
        seekbar6.setOnSeekBarChangeListener(new CircleSeekBarListener(context, score6));

        return v;
    }

    public void setThreeSetVisibility(boolean visible) {
        if (visible) {
            crd_set_3.setVisibility(View.VISIBLE);
        } else {
            crd_set_3.setVisibility(View.GONE);
        }
    }
}
