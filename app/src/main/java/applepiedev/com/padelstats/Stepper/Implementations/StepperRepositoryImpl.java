package applepiedev.com.padelstats.Stepper.Implementations;

import android.util.Log;

import java.util.List;

import applepiedev.com.padelstats.Main.events.MainActivityEvents;
import applepiedev.com.padelstats.Models.GlobalsModel;
import applepiedev.com.padelstats.Models.MatchModel;
import applepiedev.com.padelstats.Stepper.Interfaces.StepperRepository;
import applepiedev.com.padelstats.Stepper.events.StepperEvents;
import applepiedev.com.padelstats.lib.eventbus.Eventbus;
import applepiedev.com.padelstats.lib.eventbus.GreenRobotEventbus;
import applepiedev.com.padelstats.lib.retrofit.ApiCallback;
import applepiedev.com.padelstats.lib.retrofit.ApiData;
import applepiedev.com.padelstats.lib.retrofit.AppClient;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniel on 06/03/2017.
 */

public class StepperRepositoryImpl implements StepperRepository {
    public ApiData apiData = AppClient.retrofit().create(ApiData.class);
    private CompositeSubscription mCompositeSubscription;

    @Override
    public void setNewData(MatchModel matchModel, String userId) {
        String function = "addMatch";
        addSubscription(apiData.addData(function,userId,matchModel.getAtaque(),matchModel.getDefensa(),matchModel.getTransicion(),
                                            matchModel.getSaque(),matchModel.getPosicion(),matchModel.getResultado(),
                                                1,1,1),
                new ApiCallback<List<String>>() {
                    @Override
                    public void onSuccess(List<String> result) {
                        postEvent(StepperEvents.onSetNewDataSuccess);
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e("ERROR", error);
                        postEvent(StepperEvents.onSetNewDataError);
                    }

                    @Override
                    public void onFinish() {
                        // mvpView.hideLoading();
                    }
                });
    }

    public void addSubscription(Observable observable, Subscriber subscriber) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        mCompositeSubscription.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber));
    }

    // =========== Eventbus Post Events Methods ================

    private void postEvent(int type, String error) {
        MainActivityEvents mainActivityEvents = new MainActivityEvents();
        mainActivityEvents.setEventType(type);
        if (error != null) {
            mainActivityEvents.setErrorMessage(error);
        }
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(mainActivityEvents);
    }



    private void postEvent(int type) {
        StepperEvents stepperEvents = new StepperEvents();
        stepperEvents.setEventType(type);
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(stepperEvents);
    }
}
