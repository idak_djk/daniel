package applepiedev.com.padelstats.Login.implementations;

import android.util.Log;

import java.util.List;

import applepiedev.com.padelstats.Login.events.LoginEvents;
import applepiedev.com.padelstats.Login.interfaces.LoginRepository;
import applepiedev.com.padelstats.Main.Interfaces.MainActivityRepository;
import applepiedev.com.padelstats.Main.events.MainActivityEvents;
import applepiedev.com.padelstats.Models.GlobalsModel;
import applepiedev.com.padelstats.Models.LoginModel;
import applepiedev.com.padelstats.lib.eventbus.Eventbus;
import applepiedev.com.padelstats.lib.eventbus.GreenRobotEventbus;
import applepiedev.com.padelstats.lib.retrofit.ApiCallback;
import applepiedev.com.padelstats.lib.retrofit.ApiData;
import applepiedev.com.padelstats.lib.retrofit.AppClient;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Daniel on 24/02/2017.
 */

public class LoginRepositoryImpl implements LoginRepository {
    public ApiData apiData = AppClient.retrofit().create(ApiData.class);
    private CompositeSubscription mCompositeSubscription;

    @Override
    public void doLogin(String email, String pass) {
        addSubscription(apiData.doLogin(email,pass),
                new ApiCallback<List<LoginModel>>() {
                    @Override
                    public void onSuccess(List<LoginModel> result) {
                        postEvent(result);
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e("ERROR", error);
                        postEvent(LoginEvents.ondoLoginError);
                    }

                    @Override
                    public void onFinish() {
                        // mvpView.hideLoading();
                    }
                });
    }

    public void addSubscription(Observable observable, Subscriber subscriber) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        mCompositeSubscription.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber));
    }

    // =========== Eventbus Post Events Methods ================

    private void postEvent(List<LoginModel> result) {
        LoginEvents loginEvents = new LoginEvents();
            loginEvents.setEventType(LoginEvents.ondoLoginSuccess);
        loginEvents.setModel(result);
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(loginEvents);
    }

    private void postEvent(int type, List<GlobalsModel> model){
        MainActivityEvents mainActivityEvents = new MainActivityEvents();
        mainActivityEvents.setEventType(type);
        mainActivityEvents.setGlobalModel(model);
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(mainActivityEvents);
    }

    private void postEvent(int type) {
        LoginEvents loginEvents = new LoginEvents();
        loginEvents.setEventType(LoginEvents.ondoLoginError);
        Eventbus eventbus = GreenRobotEventbus.getInstance();
        eventbus.post(loginEvents);
    }
}
