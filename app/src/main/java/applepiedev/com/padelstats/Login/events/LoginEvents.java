package applepiedev.com.padelstats.Login.events;

import java.util.List;

import applepiedev.com.padelstats.Models.GlobalsModel;
import applepiedev.com.padelstats.Models.LoginModel;

/**
 * Created by Daniel on 24/02/2017.
 */

public class LoginEvents {
    public static final int ondoLoginSuccess = 1;
    public static final int ondoLoginError = 2;

    private int eventType;
    private String errorMessage;
    private List<LoginModel> model;

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setModel(List<LoginModel> model) {
        this.model = model;
    }

    public List<LoginModel> getModel() {
        return model;
    }
}

