package applepiedev.com.padelstats.Login.implementations;

import android.content.Context;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import applepiedev.com.padelstats.Login.events.LoginEvents;
import applepiedev.com.padelstats.Login.interfaces.LoginPresenter;
import applepiedev.com.padelstats.Login.interfaces.LoginRepository;
import applepiedev.com.padelstats.Login.interfaces.LoginView;
import applepiedev.com.padelstats.Models.LoginModel;
import applepiedev.com.padelstats.lib.eventbus.Eventbus;
import applepiedev.com.padelstats.lib.eventbus.GreenRobotEventbus;

/**
 * Created by Daniel on 24/02/2017.
 */

public class LoginPresenterImpl implements LoginPresenter {
    Eventbus eventbus;
    Context context;
    private LoginView loginView;
    private LoginRepository loginRepository;

    public LoginPresenterImpl(LoginView loginView, Context context) {
        this.loginView = loginView;
        this.context = context;
        this.loginRepository = new LoginRepositoryImpl();
        this.eventbus = GreenRobotEventbus.getInstance();
    }


    @Override
    public void onCreate() {
        eventbus.register(this);
    }

    @Override
    public void onDestroy() {
        eventbus.unregister(this);
    }

    @Override
    public void doLogin(String email, String pass) {
        loginRepository.doLogin(email,pass);
    }

    @Override
    public void doLoginSuccess(LoginModel model) {
        loginView.doLoginSuccess(model);
    }

    @Override
    public void doLoginError() {
        loginView.doLoginError();
    }

    @Subscribe
    @Override
    public void onEventMainThread(LoginEvents event) {
        switch (event.getEventType()) {
            case LoginEvents.ondoLoginSuccess:
                doLoginSuccess(event.getModel().get(0));
                break;
            case LoginEvents.ondoLoginError:
                doLoginError();
                break;
        }
    }
}
