package applepiedev.com.padelstats.Login.interfaces;

import java.util.List;

import applepiedev.com.padelstats.Models.LoginModel;

/**
 * Created by idak_ on 31/03/2017.
 */

public interface LoginView {



    void doLogin(String email, String pass);
    void doLoginSuccess(LoginModel model);
    void doLoginError();
}
