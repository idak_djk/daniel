package applepiedev.com.padelstats.Login.interfaces;

import java.util.List;

import applepiedev.com.padelstats.Login.events.LoginEvents;
import applepiedev.com.padelstats.Main.events.MainActivityEvents;
import applepiedev.com.padelstats.Models.GlobalsModel;
import applepiedev.com.padelstats.Models.LoginModel;

/**
 * Created by Daniel on 24/02/2017.
 */

public interface LoginPresenter {
    // Ciclo de vida de presenter
    void onCreate();
    void onDestroy();
    // Obtención de datos
    void doLogin(String email, String pass);
    void doLoginSuccess(LoginModel model);
    void doLoginError();

    // =========== Events =============
    void onEventMainThread(LoginEvents event);
}
