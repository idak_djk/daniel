package applepiedev.com.padelstats.Login.interfaces;

/**
 * Created by Daniel on 24/02/2017.
 */

public interface LoginRepository {
    // Obtención de datos
    void doLogin(String email, String pass);
}
