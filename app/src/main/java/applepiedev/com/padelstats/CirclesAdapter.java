package applepiedev.com.padelstats;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import applepiedev.com.padelstats.Circulo.CircleProgressBar;

/**
 * Created by idak_ on 06/02/2017.
 */
public class CirclesAdapter extends RecyclerView.Adapter<CirclesAdapter.ViewHolder>{
    private List<CircleProgressBar> mDataSet;
    private Context mContext;

    public CirclesAdapter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.circulo_ganado,parent,false);
        ViewHolder vh = new ViewHolder(v);

        // Return the ViewHolder
        return vh;
    }

    @Override
    public void onBindViewHolder(CirclesAdapter.ViewHolder holder, int position) {
        holder.circulo.setColor(Color.RED);
        holder.circulo.setProgress(75);
    }


    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public CirclesAdapter(Context mContext, List<CircleProgressBar> circles) {
        this.mContext = mContext;
        this.mDataSet = circles;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private final CircleProgressBar circulo;

        public ViewHolder(View v){
            super(v);
            circulo = (CircleProgressBar) v.findViewById(R.id.circulo1);
            // Get the widget reference from the custom layout

        }
    }

}
