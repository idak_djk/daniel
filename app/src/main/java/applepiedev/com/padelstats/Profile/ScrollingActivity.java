package applepiedev.com.padelstats.Profile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

import applepiedev.com.padelstats.Profile.Dialogs.PopSeekBarView;
import applepiedev.com.padelstats.Profile.Dialogs.PopSeekBarViewWeight;
import applepiedev.com.padelstats.R;

/**
 * Created by idak_ on 08/08/2016.
 */
public class ScrollingActivity extends AppCompatActivity {

    // Variables para la fecha
    private int pYear;
    private int pMonth;
    private int pDay;
    // Variables definición tipo de dialogo
    static final int NAME_DIALOG_ID = 0;
    static final int DATE_DIALOG_ID = 1;
    static final int TLF_DIALOG_ID = 2;
    static final int EMAIL_DIALOG_ID = 3;
    static final int HEIGHT_DALOG_ID = 4;
    static final int WEIGHT_DALOG_ID = 5;
    // Layouts del profile de usuario
    private RelativeLayout lytUserName;
    private RelativeLayout lytUserBirth;
    private RelativeLayout lytUserTlf;
    private RelativeLayout lytUserMail;
    private RelativeLayout lytUserHeigth;
    private RelativeLayout lytUserWeight;
    // TextView del profile de usuario
    private TextView tvUserBirth;
    private TextView tvUserName;
    private TextView tvUserTlf;
    private TextView tvUserMail;
    private TextView tvUserHeigth;
    private TextView tvUserWeight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // LAYOUT NOMBRE DE USUARIO
        lytUserName = (RelativeLayout) findViewById(R.id.lytUserName);
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        lytUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("NOMRE", "Pulsado");
                showDialog(NAME_DIALOG_ID);
            }
        });

        // LAYOUT FECHA DE NACIMIENTO DE USUARIO
        lytUserBirth = (RelativeLayout) findViewById(R.id.lytUserBirth);
        tvUserBirth = (TextView) findViewById(R.id.tvUserBirth);
        lytUserBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        // LAYOUT TELÉFONO DE USUARIO
        lytUserTlf = (RelativeLayout) findViewById(R.id.lytUserTlf);
        tvUserTlf = (TextView) findViewById(R.id.tvUserTlf);
        lytUserTlf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(TLF_DIALOG_ID);
            }
        });

        // LAYOUT EMAIL DE USUARIO
        lytUserMail = (RelativeLayout) findViewById(R.id.lytUserMail);
        tvUserMail = (TextView) findViewById(R.id.tvUserMail);
        lytUserMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(EMAIL_DIALOG_ID);
            }
        });

        // LAYOUT ALTURA DE USUARIO
        lytUserHeigth = (RelativeLayout) findViewById(R.id.lytUserHeigth);
        tvUserHeigth = (TextView) findViewById(R.id.tvUserHeigth);
        lytUserHeigth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(HEIGHT_DALOG_ID);
            }
        });

        // LAYOUT PESO DE USUARIO
        lytUserWeight = (RelativeLayout) findViewById(R.id.lytUserWeight);
        tvUserWeight = (TextView) findViewById(R.id.tvUserWeight);
        lytUserWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(WEIGHT_DALOG_ID);
            }
        });


        // OBTENEMOS FECHA ACTUAL
        final Calendar cal = Calendar.getInstance();
        pYear = cal.get(Calendar.YEAR);
        pMonth = cal.get(Calendar.MONTH);
        pDay = cal.get(Calendar.DAY_OF_MONTH);

        // LLAMAMOS AL TUTORIAL SI ES LA PRIMERA VEZ QUE LLEGAMOS AL PROFILE
        //mostartTutorial();
    }

    // GESTIÓN  DE DIALOGOS DE MODIFICACION DE VALORES
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            // Cambio de nombre de usuario
            case NAME_DIALOG_ID:
                final Dialog dialog = new Dialog(ScrollingActivity.this,R.style.DialogTheme);
                dialog.setContentView(R.layout.edit_dialog);
                dialog.setTitle("Nombre");
                Button btn_dialog_name = (Button) dialog.findViewById(R.id.dialog_ok);
                btn_dialog_name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        EditText edit = (EditText) dialog.findViewById(R.id.txtdialogo);
                        String text = edit.getText().toString();

                        dialog.dismiss();
                        Log.e("Nombre", text);
                        tvUserName.setText(text);

                    }
                });
                dialog.show();
            // Cambio de fecha de nacimiento de usuario
                break;
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,R.style.DialogTheme,
                        pDateSetListener,
                        pYear, pMonth, pDay);
            //Creacion de nuevo dialogo de texto

            case TLF_DIALOG_ID:
                final Dialog dialog_tlf = new Dialog(ScrollingActivity.this,R.style.DialogTheme);
                dialog_tlf.setContentView(R.layout.edit_dialog);
                dialog_tlf.setTitle("Teléfono");
                Button btn_dialog_tlf = (Button) dialog_tlf.findViewById(R.id.dialog_ok);
                btn_dialog_tlf.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        EditText edit = (EditText) dialog_tlf.findViewById(R.id.txtdialogo);
                        String text = edit.getText().toString();

                        dialog_tlf.dismiss();
                        Log.e("Teléfono", text);
                        tvUserTlf.setText(text);

                    }
                });
                dialog_tlf.show();
                break;
            case EMAIL_DIALOG_ID:
                final Dialog dialog_email = new Dialog(ScrollingActivity.this,R.style.DialogTheme);
                dialog_email.setContentView(R.layout.edit_dialog);
                dialog_email.setTitle("Email");
                Button btn_dialog_email = (Button) dialog_email.findViewById(R.id.dialog_ok);
                btn_dialog_email.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        EditText edit = (EditText) dialog_email.findViewById(R.id.txtdialogo);
                        String text = edit.getText().toString();

                        dialog_email.dismiss();
                        Log.e("Email", text);
                        tvUserMail.setText(text);

                    }
                });
                dialog_email.show();
                break;
            case HEIGHT_DALOG_ID:
                final Dialog dialog_altura = new Dialog(ScrollingActivity.this,R.style.DialogTheme);
                dialog_altura.setContentView(R.layout.height_dialog);
                dialog_altura.setTitle("Altura");
                final PopSeekBarView pskh = (PopSeekBarView) dialog_altura.findViewById(R.id.sb_height);
                Button btn_height_next = (Button) dialog_altura.findViewById(R.id.btn_height_next);
                btn_height_next.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        String text = pskh.getPosition() + " cm";
                        dialog_altura.dismiss();
                        Log.e("Altura", text);
                        tvUserHeigth.setText(text);
                    }
                });
                dialog_altura.show();
                break;
            case WEIGHT_DALOG_ID:
                final Dialog dialog_peso = new Dialog(ScrollingActivity.this,R.style.DialogTheme);
                dialog_peso.setContentView(R.layout.weight_dialog);
                dialog_peso.setTitle("Peso");
                final PopSeekBarViewWeight psk = (PopSeekBarViewWeight) dialog_peso.findViewById(R.id.sb_weight);
                Button btn_weight_next = (Button) dialog_peso.findViewById(R.id.btn_weight_next);
                btn_weight_next.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        String text = psk.getPosition() + " Kg";
                        dialog_peso.dismiss();
                        Log.e("Peso", text);
                        tvUserWeight.setText(text);
                    }
                });
                dialog_peso.show();
                break;
        }
        return null;
    }

    private void updateName(String nombre) {
        Log.e("NOMBRE","---->"+nombre);
    }

    //Callback recivido cuando el usuario clickea en una fecha del dialogo
    private DatePickerDialog.OnDateSetListener pDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    pYear = year;
                    pMonth = monthOfYear;
                    pDay = dayOfMonth;
                    updateDisplay();
                }
            };

    //Actualiza la fecha en el textview
    private void updateDisplay() {
        tvUserBirth.setText(
                new StringBuilder()
                        // Los meses se enumeran del 0 al 11, por lo que le sumaremos 1
                        .append(pDay).append("/")
                        .append(pMonth + 1).append("/")
                        .append(pYear));
    }

    /*private void mostartTutorial(){
        new MaterialIntroView.Builder(this)
                //Habilitar el circulo animado en el centro
                .enableDotAnimation(true)
                        //Icono de ayuda al lado del texto explicativo
                .enableIcon(false)
                .setFocusGravity(FocusGravity.CENTER)
                        //Si el boton es MATCH_PARENT.
                        //Focus.ALL no es una buena opcion. Prueba a usar
                        //Focus.MINIMUM o Focus.NORMAL.
                .setFocusType(Focus.MINIMUM)
                        //Empieza despues de 0.5 seg
                .setDelayMillis(500)
                        //Animacion aparece/desaparece con animacion fade
                .enableFadeAnimation(true)
                        //Inserta el texto explicativo
                .setInfoText("Completa tu perfil para personalizar tus resultados.")
                        //Color del texto explicativo
                .setTextColor(Color.BLACK)
                        //Tamaño texto explicativo
                        //.setInfoTextSize(30)
                        //Elige una view en el XML
                .setTarget(findViewById(R.id.lytdatos))
                        //Añade padding al circulo objetivo
                        //.setTargetPadding(30)
                        //ID debe ser unico
                .setUsageId("Profile")
                        //Color de fondo del tutorial
                        //.setMaskColor(Color.BLUE)
                        // Evento para desvanecer automaticamente el tutorial cuando el usuario haga clic en el área enfocada
                .performClick(true)
                //Si performonClick = false => Evento de escucha al pulsar
                .setListener(new MaterialIntroListener() {
                    public void onUserClicked(String materialIntroViewId) {
                        Intent i = new Intent(ScrollingActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
                }*/
    }