package applepiedev.com.padelstats.Profile;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;


import applepiedev.com.padelstats.Drawer.Profile.Dialogs.PopSeekBarView;
import applepiedev.com.padelstats.Drawer.Profile.Dialogs.PopSeekBarViewWeight;
import applepiedev.com.padelstats.R;

/**
 * Created by idak_ on 08/08/2016.
 */
public class NewScrollingActivity extends AppCompatActivity {

    // Variables para la fecha
    private int pYear;
    private int pMonth;
    private int pDay;
    // Variables definición tipo de dialogo
    static final int NAME_DIALOG_ID = 0;
    static final int DATE_DIALOG_ID = 1;
    static final int TLF_DIALOG_ID = 2;
    static final int EMAIL_DIALOG_ID = 3;
    static final int HEIGHT_DALOG_ID = 4;
    static final int WEIGHT_DALOG_ID = 5;
    // ImageView
    private ImageView imageprofile;
    // Layouts del profile de usuario
    private RelativeLayout lytUserName;
    private RelativeLayout lytUserBirth;
    private RelativeLayout lytUserTlf;
    private RelativeLayout lytUserMail;
    private RelativeLayout lytUserHeigth;
    private RelativeLayout lytUserWeight;
    // TextView del profile de usuario
    private TextView tvUserBirth;
    private TextView tvUserName;
    private TextView tvUserTlf;
    private TextView tvUserMail;
    private TextView tvUserHeigth;
    private TextView tvUserWeight;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private FloatingActionButton fabPhoto;
    public static final int GET_FROM_GALLERY = 3;
    private SharedPreferences myPrefrence;
    private SharedPreferences.Editor editor;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //handle the home button onClick event here.
                onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Detects request codes
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                imageprofile.setImageBitmap(bitmap);

                editor.putString("imageProfile", encodeTobase64(bitmap));
                editor.commit();

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.new_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageprofile = (ImageView) findViewById(R.id.imageprofile);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("Profile");

        SharedPreferences myPrefrence = PreferenceManager
                .getDefaultSharedPreferences(this);

        editor = myPrefrence.edit();
        String rName = myPrefrence.getString("name",null);
        String rBirth = myPrefrence.getString("birthday",null);
        String rPhone = myPrefrence.getString("phone",null);
        String rEmail = myPrefrence.getString("email",null);
        String rHeight = myPrefrence.getString("height",null);
        String rWeight = myPrefrence.getString("weight",null);
        if(myPrefrence.getString("imageProfile",null) != null) {
            Bitmap rImageProfile = decodeBase64(myPrefrence.getString("imageProfile",null));
            imageprofile.setImageBitmap(rImageProfile);
        }


        /*editor.putString("namePreferance", itemNAme);
        editor.putString("imagePreferance", encodeTobase64(yourbitmap));
        editor.commit();*/

        fabPhoto = (FloatingActionButton) findViewById(R.id.fabPhoto);
        fabPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);

            }
        });


        // LAYOUT NOMBRE DE USUARIO
        lytUserName = (RelativeLayout) findViewById(R.id.lytUserName);
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        if(rName != null)
            tvUserName.setText(rName);
        lytUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("NOMRE", "Pulsado");
                showDialog(NAME_DIALOG_ID);
            }
        });

        // LAYOUT FECHA DE NACIMIENTO DE USUARIO
        lytUserBirth = (RelativeLayout) findViewById(R.id.lytUserBirth);
        tvUserBirth = (TextView) findViewById(R.id.tvUserBirth);
        if(rBirth != null)
            tvUserBirth.setText(rBirth);
        lytUserBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        // LAYOUT TELÉFONO DE USUARIO
        lytUserTlf = (RelativeLayout) findViewById(R.id.lytUserTlf);
        tvUserTlf = (TextView) findViewById(R.id.tvUserTlf);
        if(rPhone != null)
            tvUserTlf.setText(rPhone);
        lytUserTlf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(TLF_DIALOG_ID);
            }
        });

        // LAYOUT EMAIL DE USUARIO
        lytUserMail = (RelativeLayout) findViewById(R.id.lytUserMail);
        tvUserMail = (TextView) findViewById(R.id.tvUserMail);
        if(rEmail != null)
            tvUserMail.setText(rEmail);
        lytUserMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(EMAIL_DIALOG_ID);
            }
        });

        // LAYOUT ALTURA DE USUARIO
        lytUserHeigth = (RelativeLayout) findViewById(R.id.lytUserHeigth);
        tvUserHeigth = (TextView) findViewById(R.id.tvUserHeigth);
        if(rHeight != null)
            tvUserHeigth.setText(rHeight);
        lytUserHeigth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(HEIGHT_DALOG_ID);
            }
        });

        // LAYOUT PESO DE USUARIO
        lytUserWeight = (RelativeLayout) findViewById(R.id.lytUserWeight);
        tvUserWeight = (TextView) findViewById(R.id.tvUserWeight);
        if(rWeight != null)
            tvUserWeight.setText(rWeight);
        lytUserWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(WEIGHT_DALOG_ID);
            }
        });


        // OBTENEMOS FECHA ACTUAL
        final Calendar cal = Calendar.getInstance();
        pYear = cal.get(Calendar.YEAR);
        pMonth = cal.get(Calendar.MONTH);
        pDay = cal.get(Calendar.DAY_OF_MONTH);

        // LLAMAMOS AL TUTORIAL SI ES LA PRIMERA VEZ QUE LLEGAMOS AL PROFILE
        //mostartTutorial();
    }

    // GESTIÓN  DE DIALOGOS DE MODIFICACION DE VALORES
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            // Cambio de nombre de usuario
            case NAME_DIALOG_ID:
                final Dialog dialog = new Dialog(NewScrollingActivity.this,R.style.DialogTheme);
                dialog.setContentView(R.layout.edit_dialog);
                dialog.setTitle("Nombre");
                Button btn_dialog_name = (Button) dialog.findViewById(R.id.dialog_ok);
                btn_dialog_name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        EditText edit = (EditText) dialog.findViewById(R.id.txtdialogo);
                        String text = edit.getText().toString();

                        editor.putString("name", text);
                        editor.commit();

                        dialog.dismiss();
                        Log.e("Nombre", text);
                        tvUserName.setText(text);

                    }
                });
                dialog.show();
            // Cambio de fecha de nacimiento de usuario
                break;
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,R.style.DialogTheme,
                        pDateSetListener,
                        pYear, pMonth, pDay);
            //Creacion de nuevo dialogo de texto

            case TLF_DIALOG_ID:
                final Dialog dialog_tlf = new Dialog(NewScrollingActivity.this,R.style.DialogTheme);
                dialog_tlf.setContentView(R.layout.edit_dialog);
                dialog_tlf.setTitle("Teléfono");
                Button btn_dialog_tlf = (Button) dialog_tlf.findViewById(R.id.dialog_ok);
                btn_dialog_tlf.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        EditText edit = (EditText) dialog_tlf.findViewById(R.id.txtdialogo);
                        String text = edit.getText().toString();

                        editor.putString("phone", text);
                        editor.commit();

                        dialog_tlf.dismiss();
                        Log.e("Teléfono", text);
                        tvUserTlf.setText(text);

                    }
                });
                dialog_tlf.show();
                break;
            case EMAIL_DIALOG_ID:
                final Dialog dialog_email = new Dialog(NewScrollingActivity.this,R.style.DialogTheme);
                dialog_email.setContentView(R.layout.edit_dialog);
                dialog_email.setTitle("Email");
                Button btn_dialog_email = (Button) dialog_email.findViewById(R.id.dialog_ok);
                btn_dialog_email.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        EditText edit = (EditText) dialog_email.findViewById(R.id.txtdialogo);
                        String text = edit.getText().toString();

                        dialog_email.dismiss();
                        Log.e("Email", text);
                        tvUserMail.setText(text);

                        editor.putString("email", text);
                        editor.commit();

                    }
                });
                dialog_email.show();
                break;
            case HEIGHT_DALOG_ID:
                final Dialog dialog_altura = new Dialog(NewScrollingActivity.this,R.style.DialogTheme);
                dialog_altura.setContentView(R.layout.height_dialog);
                dialog_altura.setTitle("Altura");
                final PopSeekBarView pskh = (PopSeekBarView) dialog_altura.findViewById(R.id.sb_height);
                Button btn_height_next = (Button) dialog_altura.findViewById(R.id.btn_height_next);
                btn_height_next.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        String text = pskh.getPosition() + " cm";
                        dialog_altura.dismiss();
                        Log.e("Altura", text);
                        tvUserHeigth.setText(text);

                        editor.putString("height", text);
                        editor.commit();
                    }
                });
                dialog_altura.show();
                break;
            case WEIGHT_DALOG_ID:
                final Dialog dialog_peso = new Dialog(NewScrollingActivity.this,R.style.DialogTheme);
                dialog_peso.setContentView(R.layout.weight_dialog);
                dialog_peso.setTitle("Peso");
                final PopSeekBarViewWeight psk = (PopSeekBarViewWeight) dialog_peso.findViewById(R.id.sb_weight);
                Button btn_weight_next = (Button) dialog_peso.findViewById(R.id.btn_weight_next);
                btn_weight_next.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        String text = psk.getPosition() + " Kg";
                        dialog_peso.dismiss();
                        Log.e("Peso", text);
                        tvUserWeight.setText(text);

                        editor.putString("weight", text);
                        editor.commit();
                    }
                });
                dialog_peso.show();
                break;
        }
        return null;
    }
    //Callback recivido cuando el usuario clickea en una fecha del dialogo
    private DatePickerDialog.OnDateSetListener pDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    pYear = year;
                    pMonth = monthOfYear;
                    pDay = dayOfMonth;
                    updateDisplay();
                }
            };

    //Actualiza la fecha en el textview
    private void updateDisplay() {
        StringBuilder birth = new StringBuilder()
                // Los meses se enumeran del 0 al 11, por lo que le sumaremos 1
                .append(pDay).append("/")
                .append(pMonth + 1).append("/")
                .append(pYear);
        tvUserBirth.setText(birth);

        editor.putString("birthday", birth.toString());
        editor.commit();
    }

    // method for bitmap to base64
    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 60, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    // method for base64 to bitmap
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    }